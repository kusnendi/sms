-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: 202.43.162.237    Database: sms
-- ------------------------------------------------------
-- Server version	5.5.35-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action_menus`
--

DROP TABLE IF EXISTS `action_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_menus` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `menu` varchar(50) NOT NULL,
  `controller` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_menus`
--

LOCK TABLES `action_menus` WRITE;
/*!40000 ALTER TABLE `action_menus` DISABLE KEYS */;
INSERT INTO `action_menus` VALUES (1,'Shooter','shooter'),(2,'Pengaturan',''),(3,'Event','event'),(4,'Report','report'),(5,'Master','');
/*!40000 ALTER TABLE `action_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_roles`
--

DROP TABLE IF EXISTS `action_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_roles` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `role_id` int(1) NOT NULL,
  `action_id` int(3) NOT NULL,
  `hide` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_roles`
--

LOCK TABLES `action_roles` WRITE;
/*!40000 ALTER TABLE `action_roles` DISABLE KEYS */;
INSERT INTO `action_roles` VALUES (2,0,1,0),(3,0,3,0),(4,0,4,0),(5,0,5,0),(6,0,6,0),(7,0,7,0),(8,0,8,0),(9,0,9,0),(10,0,10,0),(11,2,2,0),(12,2,3,0),(13,2,4,0);
/*!40000 ALTER TABLE `action_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actions`
--

DROP TABLE IF EXISTS `actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actions` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `controller` varchar(50) NOT NULL,
  `function` varchar(100) NOT NULL,
  `menu` varchar(50) NOT NULL,
  `parent` int(1) NOT NULL,
  `action_menu_id` int(3) NOT NULL,
  `no_parent` int(1) NOT NULL,
  `icon` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actions`
--

LOCK TABLES `actions` WRITE;
/*!40000 ALTER TABLE `actions` DISABLE KEYS */;
INSERT INTO `actions` VALUES (1,'shooter','','Shooter',0,0,1,'fa fa-user'),(2,'profile','','Profile',0,0,1,'fa fa-user'),(3,'event','','Event',0,0,1,'fa fa-calendar-o'),(4,'report','','Report',0,0,1,'fa fa-book'),(5,'','','Master',0,0,0,'fa fa-folder'),(6,'rifle','','Rifle',5,0,1,'fa fa-angle-double-right'),(7,'ammo','','Ammo',5,0,1,'fa fa-angle-double-right'),(8,'range','','Range',5,0,1,'fa fa-angle-double-right'),(9,'kesatuan','','Kesatuan',5,0,1,'fa fa-angle-double-right'),(10,'user','','User',5,0,1,'fa fa-angle-double-right');
/*!40000 ALTER TABLE `actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ammo`
--

DROP TABLE IF EXISTS `ammo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ammo` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `hollow_point` int(4) NOT NULL,
  `soft_point` int(4) NOT NULL,
  `ballistic_zip` int(4) NOT NULL,
  `frangible` int(4) NOT NULL,
  `tracer` int(4) NOT NULL,
  `type` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ammo`
--

LOCK TABLES `ammo` WRITE;
/*!40000 ALTER TABLE `ammo` DISABLE KEYS */;
INSERT INTO `ammo` VALUES (3,'.45ACP',0,0,0,0,0,''),(6,'9x9mm Luger',0,0,0,0,0,''),(9,'.380ACP',0,0,0,0,0,''),(10,'9x19mm Luger',0,0,0,0,0,''),(11,'5.8mm Luger',0,0,0,0,0,'');
/*!40000 ALTER TABLE `ammo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `demo`
--

DROP TABLE IF EXISTS `demo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demo` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `score` int(2) NOT NULL,
  `accuracy` int(2) NOT NULL,
  `photo` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `demo`
--

LOCK TABLES `demo` WRITE;
/*!40000 ALTER TABLE `demo` DISABLE KEYS */;
INSERT INTO `demo` VALUES (1,'Kusnendi',85,70,'avatar5.png'),(2,'John Doe',85,80,'avatar2.png'),(3,'Clarissane',95,65,'avatar4.png');
/*!40000 ALTER TABLE `demo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `rifle_id` int(4) NOT NULL,
  `ammo_id` int(4) NOT NULL,
  `range_id` int(3) NOT NULL,
  `max_shots` int(2) NOT NULL,
  `deskripsi` text NOT NULL,
  `type` int(1) DEFAULT NULL,
  `mode_id` int(4) NOT NULL,
  `user_id` int(6) NOT NULL,
  `create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_public` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rifle_id` (`rifle_id`,`ammo_id`,`range_id`,`max_shots`),
  KEY `ammo_id` (`ammo_id`),
  KEY `range_id` (`range_id`),
  CONSTRAINT `event_range_id` FOREIGN KEY (`range_id`) REFERENCES `range` (`id`),
  CONSTRAINT `event_rifle_id` FOREIGN KEY (`rifle_id`) REFERENCES `rifle` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (2,'Shots Competition',1,2,1,10,'Kompetisi tembak jitu TNI Hut ke 51 tahun',NULL,0,0,'2014-08-22 16:10:18',0,0),(3,'HUT RI 69 Merdeka',1,2,1,10,'Kompetisi penembak jitu HUT RI ke 69 thn',NULL,0,0,'2014-08-22 16:10:18',0,0),(4,'Outing ACE 2014',5,2,1,10,'Shoots Competition on ACE 1st Anniversary',NULL,0,0,'2014-08-25 12:01:23',0,0),(5,'Test Demo Event',5,2,1,10,'Demo Events Coba Coba Dulu...',NULL,0,12,'2014-09-05 16:13:31',0,1),(6,'Demo 1 - Sunue',7,3,1,10,'Demo Shooter Apps di Bea Cukai, Generate 1',NULL,0,6,'2014-09-11 11:26:27',0,0);
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kesatuan`
--

DROP TABLE IF EXISTS `kesatuan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kesatuan` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kesatuan`
--

LOCK TABLES `kesatuan` WRITE;
/*!40000 ALTER TABLE `kesatuan` DISABLE KEYS */;
INSERT INTO `kesatuan` VALUES (1,'ACE','Jl. Veteran III Jakarta, Indonesia'),(3,'TNI - AD','Jl. Perintis Kemerdekaan No. 65, Jakarta'),(4,'TNI - AL',''),(5,'TNI - AU','');
/*!40000 ALTER TABLE `kesatuan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `match`
--

DROP TABLE IF EXISTS `match`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `match` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `event_id` int(4) NOT NULL,
  `shooter_id` int(6) NOT NULL,
  `camera_ip` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `event_id` (`event_id`,`shooter_id`),
  KEY `shooter_id` (`shooter_id`),
  CONSTRAINT `match_event_id` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `match`
--

LOCK TABLES `match` WRITE;
/*!40000 ALTER TABLE `match` DISABLE KEYS */;
INSERT INTO `match` VALUES (1,2,11,''),(2,3,11,''),(3,3,3,''),(4,4,11,''),(5,4,3,''),(6,2,2,''),(7,5,12,'192.168.1.10');
/*!40000 ALTER TABLE `match` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mode`
--

DROP TABLE IF EXISTS `mode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mode` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `catatan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mode`
--

LOCK TABLES `mode` WRITE;
/*!40000 ALTER TABLE `mode` DISABLE KEYS */;
INSERT INTO `mode` VALUES (1,'Deathmatch','-'),(2,'Quick Shots','Fastest Shots Mode');
/*!40000 ALTER TABLE `mode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `range`
--

DROP TABLE IF EXISTS `range`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `range` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `jarak` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `range`
--

LOCK TABLES `range` WRITE;
/*!40000 ALTER TABLE `range` DISABLE KEYS */;
INSERT INTO `range` VALUES (1,'',50);
/*!40000 ALTER TABLE `range` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `result` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `match_id` int(6) NOT NULL,
  `no_urut` int(2) NOT NULL,
  `nilai` int(2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `match_id` (`match_id`),
  CONSTRAINT `result_match_id` FOREIGN KEY (`match_id`) REFERENCES `match` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result`
--

LOCK TABLES `result` WRITE;
/*!40000 ALTER TABLE `result` DISABLE KEYS */;
INSERT INTO `result` VALUES (1,1,1,7,'2014-08-25 10:24:01',''),(2,1,2,8,'2014-08-25 10:24:04',''),(3,1,3,9,'2014-08-25 10:24:07',''),(4,1,4,8,'2014-08-25 10:24:11',''),(5,1,5,8,'2014-08-25 10:24:15',''),(6,2,1,7,'2014-08-24 05:36:53',''),(7,2,2,8,'2014-08-24 05:42:56',''),(8,2,3,7,'2014-08-24 05:47:46',''),(9,2,4,8,'2014-08-24 05:52:56',''),(10,2,5,8,'2014-08-24 06:02:31',''),(11,3,1,8,'2014-08-24 05:36:53',''),(12,3,2,8,'2014-08-24 05:42:56',''),(13,3,3,9,'2014-08-24 05:47:46',''),(14,3,4,8,'2014-08-24 05:52:56',''),(15,3,5,8,'2014-08-24 06:02:31',''),(16,4,1,8,'2014-08-25 12:15:11',''),(17,4,2,9,'2014-08-25 12:15:41',''),(18,4,3,8,'2014-08-25 12:16:05',''),(19,4,4,9,'2014-08-25 12:16:33',''),(20,4,5,9,'2014-08-25 12:18:15',''),(21,6,1,10,'2014-09-11 10:28:03',''),(22,6,2,9,'2014-09-11 10:28:03',''),(23,6,3,8,'2014-09-11 10:28:03',''),(24,6,4,10,'2014-09-11 10:28:03',''),(25,6,5,9,'2014-09-11 10:28:03',''),(26,7,0,90,'0000-00-00 00:00:00','ads3.jpg'),(27,7,0,90,'2014-09-24 13:33:06',''),(28,7,0,100,'2014-09-24 13:33:51','10462905_296891287101542_8379142001381317626_n.jpg'),(29,7,0,75,'2014-09-24 14:20:40','10462905_296891287101542_8379142001381317626_n.jpg'),(30,7,0,85,'2014-09-24 14:23:46','img1.png'),(31,7,0,95,'2014-09-24 14:24:19','img4.png'),(32,7,0,75,'2014-09-24 14:27:30','img3.png');
/*!40000 ALTER TABLE `result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rifle`
--

DROP TABLE IF EXISTS `rifle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rifle` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `caliber` varchar(30) NOT NULL,
  `weight` varchar(6) NOT NULL,
  `length` varchar(6) NOT NULL,
  `barrel_length` varchar(6) NOT NULL,
  `capacity` varchar(4) NOT NULL,
  `picture` varchar(50) NOT NULL DEFAULT '1287738431_thum.jpg',
  `note` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rifle`
--

LOCK TABLES `rifle` WRITE;
/*!40000 ALTER TABLE `rifle` DISABLE KEYS */;
INSERT INTO `rifle` VALUES (1,'SBR50','9x19mm Luger','n/a','n/a','n/a','n/a','1287725579_thum.jpg','Standing Rifle 50m'),(2,'Colt SSP','9x9mm Luger','n/a','n/a','n/a','15','1287742032_thum.jpg',''),(3,'Heckler-Koch HK-4','9x17/.380ACP','520','157','85','8','1287725579_thum.jpg',''),(4,'Star 30M','9x19mm Luger','1140','205','119','15','1287738431_thum.jpg',''),(5,'QSZ-92','5.8mm','760','190','0','15','1287743613_thum.jpg',''),(6,'Steyr GB','9x19mm Luger','845','216','136','18','1287742228_thum.jpg',''),(7,'FNP-45','.45ACP','930','200','114','14','1287672040_thum.jpg',''),(8,'Glock','.45ACP','745','209','117','13','1287738431_thum.jpg',''),(9,'Yavuz 16','9x19mm Luger','850','217','125','15','1287742228_thum.jpg',''),(10,'FEG P9RK','9x19mm Luger','1170','203','118','14','1287743613_thum.jpg',''),(11,'Colt Double Eagle','.45ACP','1205','216','127','8','1287742228_thum.jpg','');
/*!40000 ALTER TABLE `rifle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shooter`
--

DROP TABLE IF EXISTS `shooter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shooter` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `user_id` int(6) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `handphone` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `kesatuan_id` int(4) NOT NULL,
  `picture` varchar(50) NOT NULL DEFAULT 'default.gif',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `user_id_2` (`user_id`),
  KEY `kesatuan_id` (`kesatuan_id`),
  CONSTRAINT `id_kesatuan_shooter` FOREIGN KEY (`kesatuan_id`) REFERENCES `kesatuan` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shooter`
--

LOCK TABLES `shooter` WRITE;
/*!40000 ALTER TABLE `shooter` DISABLE KEYS */;
INSERT INTO `shooter` VALUES (2,2,'Kusnendi','nendi@ace.co.id','08992332262','Jl. Koramil No 65 Cicurug Sukabumi',1,'avatar3.jpg'),(3,3,'Aga Wibowo','aga.wibowo@gmail.com','08991234000','Jl. Suryopranoto 2 Komplek Plaza Harmoni F8',1,'avatar5.png'),(10,10,'M Sulton','sulton@ace.co.id','08221100510','Jl. Suryopranoto 2 Komplek Plaza Harmoni F8',3,'avatar4.png'),(11,12,'John Doe','nendi@ace.co.id','08987063782','Jl. Suryopranoto 2 Komplek Plaza Harmoni F8',1,'default.gif'),(12,13,'Sunue','sunue@yahoo.com','08221100000','',5,'default.gif'),(13,0,'Dudi','dudi@ace.co.id','089912340000','Jl. Suryopranoto 2 Komplek Plaza Harmoni F8',1,'default.gif');
/*!40000 ALTER TABLE `shooter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `username` varchar(8) CHARACTER SET latin1 NOT NULL,
  `password` varchar(32) CHARACTER SET latin1 NOT NULL,
  `fullname` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `role_id` int(1) NOT NULL,
  `sid` varchar(32) CHARACTER SET latin1 NOT NULL DEFAULT 't4rn5eig126nbrf7ajt',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `display_picture` varchar(50) NOT NULL DEFAULT 'default.gif',
  `slug` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (6,'nendi','cfba03cfe4c900cf8069e518fd420c17','Kusnendi',0,'9oomosqi0divl0be2iugmjc7o3',1,'avatar3.jpg',NULL),(9,'wirya','bbdab75773b186191884a2e4bc44199c','Wiryanata',0,'tf75rbqaljchgrbd2s9f8qabc0',1,'avatar5.png',NULL),(10,'sulton','8572cf8e1738b92c7de35ec2ba7e9ec2','M Sulton',2,'r3ubj6bvjglp0pt8hfserrl9p7',1,'avatar4.png',NULL),(11,'admin','21232f297a57a5a743894a0e4a801fc3',NULL,0,'uu09sj0msbs00npk0d1c6vutt5',1,'default.gif',NULL),(12,'demo','fe01ce2a7fbac8fafaed7c982a04e229',NULL,2,'5btqdo2tr5ec6e1peevfpq2qd3',1,'default.gif',NULL),(13,'sunue','5f4dcc3b5aa765d61d8327deb882cf99',NULL,2,'rivi5u54hl0mq429mfl670qos5',1,'default.gif',NULL),(14,'rest','fe01ce2a7fbac8fafaed7c982a04e229',NULL,0,'t4rn5eig126nbrf7ajt',1,'default.gif',NULL),(15,'rest2','1066726e7160bd9c987c9968e0cc275a',NULL,0,'t4rn5eig126nbrf7ajt',1,'420.jpg',NULL),(16,'rest3','297e430d45e7bf6f65f5dc929d6b072b',NULL,0,'t4rn5eig126nbrf7ajt',1,'420.jpg',NULL),(17,'rest4','7b1312a1b3e74bb174b3fbbf68ab5a92',NULL,0,'t4rn5eig126nbrf7ajt',1,'420.jpg',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warming`
--

DROP TABLE IF EXISTS `warming`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warming` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `match_id` int(6) NOT NULL,
  `no_urut` int(2) NOT NULL,
  `nilai` int(2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warming`
--

LOCK TABLES `warming` WRITE;
/*!40000 ALTER TABLE `warming` DISABLE KEYS */;
INSERT INTO `warming` VALUES (1,5,1,9,'2014-08-31 06:04:17',''),(2,5,2,8,'2014-08-31 06:04:32',''),(3,5,3,8,'2014-08-31 06:04:45',''),(4,5,4,9,'2014-08-31 06:05:00',''),(5,5,5,9,'2014-08-31 06:05:16','');
/*!40000 ALTER TABLE `warming` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-09-25 12:10:11
