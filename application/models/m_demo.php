<?php
class M_demo extends CI_Model{
	/*** Get All Data ***/
	public function getData(){
        $q = $this->db->get('demo');
		return $q->result();
	}
	
	public function getDatabyID($match_id,$shooter_id){
		$this->db->where('match_id',$match_id);
		$q = $this->db->get('result');
		return $q->result();
	}

    public function arrBoxType(){
        $data = array(
            '1' => 'box-primary',
            '2' => 'box-info',
            '3' => 'box-danger'
        );
        return $data;
    }
}