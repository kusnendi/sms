<?php
class M_user extends CI_Model{
	/*** Get All Data
	public function getData($num,$offset){
		$this->db->order_by('id','desc');
		$q = $this->db->get('user',$num,$offset);
		return $q->result();
	}
     ***/
    public function getAll($num,$offset){
        $q = $this->db->get('user',$num,$offset);
        return $q->result();
    }

    /*** Get data by id ***/
    public function getbyID($id,$sid){
        $this->db->where('id',$id);
        $this->db->where('sid',$sid);
        $q = $this->db->get('user');
        return $q->result();
    }

    public function search($param,$search){
        $this->db->where($param,$search);
        $q = $this->db->get('user');
        if ($q->num_rows() > 0) return $q->result();
    }

    public function arrRole(){
        $data = array(
            '0' => 'Admin',
            '1' => 'Instructor',
            '2' => 'User'
        );
        return $data;
    }

    public function arrStatus(){
        $data = array(
            '0' => 'Not Active',
            '1' => 'Active'
        );
        return $data;
    }

    public function arrLabel(){
        $data = array(
            '0' => 'label-danger',
            '1' => 'label-info'
        );
        return $data;
    }

}