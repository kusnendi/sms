<?php
class M_combobox extends CI_Model{
	/*** Get Kota ***/
	function getKota(){
		$this->db->order_by('ID_KOTA','ASC');
		$q = $this->db->get('kota');
		if ($q->num_rows() > 0)	return $q->result();
	}
	
	/*** Get Industri ***/
	function getIndustri(){
		$this->db->order_by('ID_INDUSTRY','ASC');
		$q = $this->db->get('industry');
		if ($q->num_rows() > 0) return $q->result();
	}
}