<?php
class M_rifle extends CI_Model{
	/*** Get All Data ***/
	public function getData($num,$offset){
		$this->db->order_by('id','desc');
		$q = $this->db->get('rifle',$num,$offset);
		return $q->result();
	}
	
	public function getDatabyID($id){
		$this->db->where('id',$id);
		$q = $this->db->get('rifle');
		return $q->result();
	}
	
	public function getbyName($name){
		$this->db->where('jenis',$name);
		$q = $this->db->get('rifle');
		return $q->result();
	} 
	
	function cariData($keyword){
		$this->db->like('jenis',$keyword);
		$q = $this->db->get('rifle');
		if ($q->num_rows() > 0) return $q->result();
	}
}