<?php
class M_ammo extends CI_Model{
	/*** Get All Data ***/
	public function getData($num,$offset){
		//$sql = "select t1.*,t2.senapan from ammo t1 left join rifle t2 on t1.rifle_id=t2.id limit $offset,$num";
		$q   = $this->db->get('ammo',$num,$offset);
		return $q->result();
	}
	
	public function getDatabyID($id){
		$this->db->where('id',$id);
		$q = $this->db->get('ammo');
		return $q->result();
	}
	
	public function getbyRifle($id){
		$this->db->where('rifle_id',$id);
		$q = $this->db->get('ammo');
		return $q->result();
	} 
}