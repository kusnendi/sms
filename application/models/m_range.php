<?php
class M_range extends CI_Model{
	/*** Get All Data ***/
	public function getData($num,$offset){
		$this->db->order_by('id','desc');
		$q = $this->db->get('range',$num,$offset);
		return $q->result();
	}
	
	public function getDatabyID($id){
		$this->db->where('id',$id);
		$q = $this->db->get('range');
		return $q->result();
	}
	
	public function getbyName($name){
		$this->db->where('jarak',$name);
		$q = $this->db->get('range');
		return $q->result();
	}
}