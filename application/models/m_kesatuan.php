<?php
class M_kesatuan extends CI_Model{
	/*** Get All Data ***/
	public function getData($num,$offset){
		$this->db->order_by('id','desc');
		$q = $this->db->get('kesatuan',$num,$offset);
		return $q->result();
	}
	
	public function getDatabyID($id){
		$this->db->where('id',$id);
		$q = $this->db->get('kesatuan');
		return $q->result();
	}
	
	public function getbyName($name){
		$this->db->where('nama',$name);
		$q = $this->db->get('kesatuan');
		return $q->result();
	} 
	
	function cariData($keyword){
		$this->db->like('nama',$keyword);
		$q = $this->db->get('kesatuan');
		if ($q->num_rows() > 0) return $q->result();
	}
}