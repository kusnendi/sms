<?php
class M_report extends CI_Model{
	/*** Get All Data ***/
	public function getData($shooter_id,$caliber,$rifle,$startdate,$endate,$code){
        //getData($view,$name,$caliber,$rifle,$startdate,$endate)

        /*** Filtering Field ***/
		$this->db->where('shooter_id',$shooter_id);
        if (!empty($caliber) != 'Any'){
            $this->db->where('ammo_id',$caliber);
        }
        if (!empty($rifle) != 'Any'){
            $this->db->where('rifle_id',$caliber);
        }
        if (!empty($startdate)){
            $this->db->where('`date` >=',$startdate);
        }
        if (!empty($endate)){
            $this->db->where('`date` <=',$endate);
        }

		/*** Data Return ***/
        if ($code == 1){
            $q = $this->db->get('review');
            foreach ($q->result() as $row){
                $data[] = $row->score;
            }
            return $data;
        } elseif ($code == 2){
            $q = $this->db->get('review');
            foreach ($q->result() as $row){
                $data[] = $row->score;
            }
            return $data;
        }
        else {
            $q = $this->db->get('review');
            return $q->result();
        }
	}

    public function jsontest(){
        $this->db->select('score');
        $q = $this->db->get('review');
        foreach ($q->result() as $row){
            $data[] = $row->score;
        }
        return $data;
    }

    public function getShooter($shooter_id,$caliber,$rifle,$startdate,$endate,$code){
        //$sql = "select t1.*,t2.nama_lengkap,t2.picture,t3.nama as kesatuan,sum(t4.nilai) as nilai,sum(t4.nilai)/count(t4.id) as accurated from `match` t1 left join shooter t2 on t1.shooter_id=t2.id left join kesatuan t3 on t2.kesatuan_id=t3.id left join result t4 on t1.id=t4.match_id where t1.event_id=$event_id group by t1.id";
        //$sql = "select t1.*,t2.nama_lengkap,t3.nama as kesatuan from `match` t1 left join shooter t2 on t1.shooter_id=t2.id left join kesatuan t3 on t2.kesatuan_id=t3.id where t1.event_id=$event_id";
        /*** Filtering Field ***/

        $this->db->select('t6.*,t1.*,t2.nama_lengkap,t2.picture,t3.nama as kesatuan,sum(t4.nilai) as score,sum(t4.nilai)/(count(t4.id)*10)*100 as accurate,sum(t5.nilai) as score2,sum(t5.nilai)/(count(t5.id)*10)*100 as accurate2');
        $this->db->from('`match` t1');
        $this->db->join('event t6','t1.event_id=t6.id','left');
        $this->db->join('shooter t2','t1.shooter_id=t2.id','left');
        $this->db->join('kesatuan t3','t2.kesatuan_id=t3.id','left');
        $this->db->join('result t4','t1.id=t4.match_id','left');
        $this->db->join('warming t5','t1.id=t5.match_id','left');
        /*** condition ***/
        $this->db->where('t2.id',$shooter_id);
        if (!empty($caliber) != 'Any'){
            $this->db->where('ammo_id',$caliber);
        }
        if (!empty($rifle) != 'Any'){
            $this->db->where('rifle_id',$caliber);
        }
        if (!empty($startdate)){
            $this->db->where("STR_TO_DATE(left(t6.create,10), '%Y-%m-%d') >=",$startdate);
        }
        if (!empty($endate)){
            $this->db->where("STR_TO_DATE(left(t6.create,10), '%Y-%m-%d') <=",$endate);
        }
        $this->db->group_by('t1.id');

        //$r   = $this->db->query($sql);
        $r = $this->db->get();

        if ($code == 1){
            return $r->result();
        } elseif ($code == 2) { //real score
            if ($r->num_rows > 0){
                foreach ($r->result() as $data){
                    $row[] = $data->score;
                }
                return $row;
            }

        } elseif ($code == 3) { //real accurate
            if ($r->num_rows > 0){
                foreach ($r->result() as $data){
                    $row[] = $data->accurate;
                }
                return $row;
            }

        } elseif ($code == 4) { //real accurate
            if ($r->num_rows > 0){
                foreach ($r->result() as $data){
                    $row[] = $data->score2;
                }
                return $row;
            }

        } elseif ($code == 5) { //real accurate
            if ($r->num_rows > 0){
                foreach ($r->result() as $data){
                    $row[] = $data->accurate2;
                }
                return $row;
            }

        } else {
            return $r->result();
        }

    }

}