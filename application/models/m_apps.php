<?php
class M_apps extends CI_Model{
	/*** Get Data ***/
	public function getData($table){
		$q = $this->db->get($table);
		return $q->result();
	}
	
	/*** Simpan Data ***/
	public function simpanData($table,$data){
		$this->db->insert($table,$data);
		return;
	}
	/*** Simpan Data ***/
	public function updateData($table,$id,$data){
		$this->db->where('id',$id);
		$this->db->update($table,$data);
		return;
	}
	/*** Hapus Data ***/
	public function delData($table,$id){
		$this->db->where('id',$id);
		$this->db->delete($table);
		return;
	}
}