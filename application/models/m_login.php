<?php
class M_login extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	
	//fungsi anti-injeksi
	function anti_injeksi($data){
		$filter = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
		return $filter;
	}
	
	function cek_user($user,$password){
		$this->db->where('USERNAME', $user);
		$this->db->where('PASSWORD', $password);
		$this->db->where('IS_ACTIVE', 1);
		$query = $this->db->get('user');
		
		if ($query->num_rows == 1){
			return true;
		}
	}
	
}
?>