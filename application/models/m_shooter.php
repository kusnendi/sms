<?php
class M_shooter extends CI_Model{
	/*** Get All Data
	public function getData($num,$offset){
		$this->db->order_by('id','desc');
		$q = $this->db->get('shooter',$num,$offset);
		return $q->result();
	}
     ***/
    public function getData($num,$offset){
        $sql = "select t1.*,t2.nama as kesatuan from shooter t1 left join kesatuan t2 on t1.kesatuan_id=t2.id limit $offset,$num";
        $exe = $this->db->query($sql);
        return $exe->result();
    }

    /*** Get data by id ***/
    public function getDatabyID($id){
            $this->db->select('t1.*,t2.nama as kesatuan');
            $this->db->join('kesatuan t2','t1.kesatuan_id=t2.id','left');
            $this->db->where('t1.id',$id);
            $q = $this->db->get('shooter t1');
            return $q->result();
    }

    public function getbyUserID($uid){
            $this->db->where('user_id',$uid);
            $q = $this->db->get('shooter');
            return $q->result();
    }

    public function getbyName($name){
            $this->db->select('t1.*,t2.nama as kesatuan');
            $this->db->join('kesatuan t2','t1.kesatuan_id=t2.id','left');
            $this->db->where('t1.nama_lengkap',$name);
            $q = $this->db->get('shooter t1');
            return $q->result();
    } 

    function cariData($keyword){
            $this->db->select('t1.*,t2.nama as kesatuan');
            $this->db->join('kesatuan t2','t1.kesatuan_id=t2.id','left');
            $this->db->like('nama_lengkap',$keyword);
            $q = $this->db->get('shooter t1');
            if ($q->num_rows() > 0) return $q->result();
    }
}