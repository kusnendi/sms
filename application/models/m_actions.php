<?php
/**
 * Created by PhpStorm.
 * User: Adicipta
 * Date: 05/09/14
 * Time: 10:17
 */

class M_actions extends CI_Model {
    public function getParentActions($role_id){
        $this->db->select('t1.*,t2.role_id,t2.hide');
        $this->db->join('action_roles t2','t1.id=t2.action_id','left');
        $this->db->where('t2.role_id',$role_id);
        $this->db->where('t1.parent',0);
        $q = $this->db->get('actions t1');
        return $q->result();
    }

    public function getSubActions($role_id){
        $this->db->select('t1.*,t2.role_id,t2.hide');
        $this->db->join('action_roles t2','t1.id=t2.action_id','left');
        $this->db->where('t2.role_id',$role_id);
        $this->db->where('t1.parent <>',0);
        $q = $this->db->get('actions t1');
        return $q->result();
    }
} 