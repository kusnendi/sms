<?php
class M_result extends CI_Model{
	/*** Get All Data ***/
	public function getData($mid){
        $sql = "select t1.event_id,t1.shooter_id,t2.* from `match` t1 left join result t2 on t1.id=t2.match_id where t1.id=$mid";
        $q   = $this->db->query($sql);
		return $q->result();
	}
	
	public function getDatabyID($match_id,$shooter_id){
		$this->db->where('match_id',$match_id);
		$q = $this->db->get('result');
		return $q->result();
	}
}