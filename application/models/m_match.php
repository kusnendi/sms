<?php
class M_match extends CI_Model{
	/*** Get All Data ***/
	public function getData($num,$offset){
		$this->db->order_by('id','desc');
		$q = $this->db->get('match');
		return $q->result();
	}

	/*** Get Shooter by Event ID ***/
    public function getShooter($event_id){
        //$sql = "select t1.*,t2.nama_lengkap,t2.picture,t3.nama as kesatuan,sum(t4.nilai) as nilai,sum(t4.nilai)/count(t4.id) as accurated from `match` t1 left join shooter t2 on t1.shooter_id=t2.id left join kesatuan t3 on t2.kesatuan_id=t3.id left join result t4 on t1.id=t4.match_id where t1.event_id=$event_id group by t1.id";
        //$sql = "select t1.*,t2.nama_lengkap,t3.nama as kesatuan from `match` t1 left join shooter t2 on t1.shooter_id=t2.id left join kesatuan t3 on t2.kesatuan_id=t3.id where t1.event_id=$event_id";
        $this->db->select('t1.*,t2.nama_lengkap,t2.picture,t3.nama as kesatuan,sum(t4.nilai) as score,sum(t4.nilai)/(count(t4.id)*10)*100 as accurate,sum(t5.nilai) as score2,sum(t5.nilai)/(count(t5.id)*10)*100 as accurate2');
        $this->db->from('`match` t1');
        $this->db->join('shooter t2','t1.shooter_id=t2.id','left');
        $this->db->join('kesatuan t3','t2.kesatuan_id=t3.id','left');
        $this->db->join('result t4','t1.id=t4.match_id','left');
        $this->db->join('warming t5','t1.id=t5.match_id','left');
        $this->db->where('t1.event_id',$event_id);
        $this->db->group_by('t1.id');
        //$r   = $this->db->query($sql);
        $r = $this->db->get();
        return $r->result();
    }

    public function cekbyUID($eid,$uid){
        $this->db->where('id',$eid);
        $this->db->where('user_id',$uid);
        $q = $this->db->get('`match`');
        if ($q->num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*** Get Shooter by Match ID ***/
    public function getShooterMatchID($match_id){
        $sql = "select t1.*,t2.nama_lengkap,t3.nama as kesatuan from `match` t1 left join shooter t2 on t1.shooter_id=t2.id left join kesatuan t3 on t2.kesatuan_id=t3.id where t1.id=$match_id";
        $r   = $this->db->query($sql);
        return $r->result();
    }
    
    public function getShooterbyIP($ip){
        $this->db->select('t1.*,t2.status');
        $this->db->from('`match` t1');
        $this->db->join('event t2','t1.event_id=t2.id','left');
        $this->db->where('t1.camera_ip',$ip);
        $this->db->where('t2.status',1);
        $q = $this->db->get();
        
        if ($q->num_rows > 0)
        {
            //return $q->row();
            return $q->row();
        }
        else 
        {
            return FALSE;
        }
    }
    
    public function getDemo(){
        $query = "select t1.*,t2.status from `match` t1 left join event t2 on t1.event_id=t2.id where t1.camera_ip='192.168.1.10' and t2.status=1";
        $q     = $this->db->query($query);
        return $q->row();
    }
        
}