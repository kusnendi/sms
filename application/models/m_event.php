<?php
class M_event extends CI_Model{
	/*** Get All Data ***/
	public function getData($num,$offset){
		$sql = "select t1.*,count(t2.shooter_id) as shooter from `event` `t1` left join `match` `t2` on t1.id=t2.event_id group by t1.id limit $offset,$num";
		$q   = $this->db->query($sql);
		return $q->result();
	}
	
	public function getDatabyID($id){
        $sql = "select t1.*,t2.name as senapan,t3.name as ammo,t4.jarak from event t1 left join rifle t2 on t1.rifle_id=t2.id left join ammo t3 on t1.ammo_id=t3.id left join `range` t4 on t1.range_id=t4.id where t1.id=$id";
		$q   = $this->db->query($sql);
		return $q->result();
	}

    public function getbyUserID($uid,$num,$offset){
        $this->db->select('t1.*,t2.name as senapan,t3.type as ammo,t4.jarak');
        $this->db->join('rifle t2','t1.rifle_id=t2.id','left');
        $this->db->join('ammo t3','t1.ammo_id=t3.id','left');
        $this->db->join('range t4','t1.range_id=t4.id','left');
        $this->db->where('t1.user_id',$uid);
        $this->db->or_where('t1.user_id',0);
        $q = $this->db->get('event t1',$num,$offset);
        return $q->result();
    }
	
	public function getbyName($name){
		$this->db->where('judul',$name);
		$q = $this->db->get('event');
		return $q->result();
	} 
	
	function cariData($keyword){
        $this->db->select('t1.*,count(t2.shooter_id) as shooter');
        $this->db->join('`match` t2','t1.id=t2.event_id','left');
        $this->db->group_by('t1.id');
		$this->db->like('judul',$keyword);
		$q = $this->db->get('event t1');
		if ($q->num_rows() > 0) return $q->result();
	}

    public function arrType(){
        $data = array(
            '1' => 'Competition',
            '2' => 'Other'
        );
        return $data;
    }

    public function arrStatus(){
        $data = array(
            '1' => 'Open',
            '2' => 'Started',
            '3' => 'Closed'
        );
        return $data;
    }
}