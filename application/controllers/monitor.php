<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Monitor extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		session_start();
		parent::__construct();
        $this->load->model('m_demo');
		/* cek login
		if (!isset($_SESSION['username'])){
			redirect('login');
		} else {
			/* cek user role
			$this->role 	= $_SESSION['role'];
			$this->username = $_SESSION['username'];
            $this->Actions  = $this->m_actions->getParentActions($this->role);
            $this->SubAction= $this->m_actions->getSubActions($this->role);
		}
		*/
	}
	

	public function index($id=NULL)
	{
		$this->load->view('pages/monitor/index');
		$this->load->view('elements/footer');
	}

    public function ajaxrequest(){
        $data = array(
            'getData'   => $this->m_demo->getData(),
            'BoxType'   => $this->m_demo->arrBoxType()
        );

        $this->load->view('pages/monitor/ajax',$data);
    }
	
	public function json(){
		$data = $this->m_kartu_nama->getDatabyID(6);
		
		echo json_encode($data);
	}

    public function unit_test(){
        echo $_SESSION['display'];
    }

}