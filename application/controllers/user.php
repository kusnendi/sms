<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    function __construct(){
        session_start();
        parent::__construct();
        /* cek login */
        if (!isset($_SESSION['username'])){
            redirect('login');
        } else {
            /* cek user role */
            $this->role 	= $_SESSION['role'];
            $this->username = $_SESSION['username'];
            $this->Actions  = $this->m_actions->getParentActions($this->role);
            $this->SubAction= $this->m_actions->getSubActions($this->role);
        }
    }
	
	private function paging($table,$url,$per_page,$numlinks) {
		$jml		= $this->db->get($table);
	
		//pengaturan pagination
		$config['base_url'] 	= $url;
		$config['total_rows'] 	= $jml->num_rows();
		$config['num_links']	= $numlinks;
		$config['per_page'] 	= $per_page;
		$config['full_tag_open'] = "<ul class='pagination pagination-sm'>";
		$config['full_tag_close'] = '</ul>';
		$config['first_tag_open'] = '<li>';
		$config['first_page'] 	= 'Awal';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_page'] 	= 'Akhir';
		$config['last_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_page'] 	= 'Next';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_page'] 	= 'Prev';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class=active><a href=#>';
		$config['cur_tag_close'] = '</a></li>';
	
		//inisialisasi config
		$this->pagination->initialize($config);
	}
	
	public function index($id=0)
	{
		//pengaturan pagination
		$table 		= 'user';
		$url   		= base_url('user/index');
		$numlinks	= 2;
		$per_page	= 20;
		
		$this->paging($table, $url, $per_page, $numlinks);
		
		$data = array(
				'hal'		    => $this->pagination->create_links(),
				'num'		    => $id,
                'Actions'       => $this->Actions,
                'SubActions'    => $this->SubAction,
				'getData'	    => $this->m_user->getAll($per_page,$id),
                'Role'          => $this->m_user->arrRole(),
                'Status'        => $this->m_user->arrStatus(),
                'Label'         => $this->m_user->arrLabel()
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/user/index');
		$this->load->view('elements/footer');
	}
	
	public function add(){
		$data = array(
                'Actions'       => $this->Actions,
                'SubActions'    => $this->SubAction,
                'Role'          => $this->m_user->arrRole(),
                'Status'        => $this->m_user->arrStatus()
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/user/tambah');
		$this->load->view('elements/footer');
	}
	
	public function edit($id,$sid){
		$data = array(
                'Actions'       => $this->Actions,
                'SubActions'    => $this->SubAction,
				'getData'	    => $this->m_user->getbyID($id,$sid),
                'Role'          => $this->m_user->arrRole(),
                'Status'        => $this->m_user->arrStatus()
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/user/edit');
		$this->load->view('elements/footer');
	}
	
	public function simpan(){
		
		$data = array(
				'username'	=> $this->input->post('username'),
				'password'	=> md5($this->input->post('password')),
				'role_id'	=> $this->input->post('role_id'),
				'is_active'	=> $this->input->post('is_active')
		);
		/*** Simpan Data ke Database ***/
		$this->m_apps->simpanData('user',$data);
		redirect('user?msg_id=402');
	}
	
	public function update(){
		$id       = $this->input->post('id');
        $password = $this->input->post('password');
		/*** Cek Password ***/
        if (empty($password)){
            $data = array(
                'username'	    => $this->input->post('username'),
                'role_id'		=> $this->input->post('role_id'),
                'is_active'		=> $this->input->post('is_active')
            );
        } else {
            $data = array(
                'username'	    => $this->input->post('username'),
                'password'      => md5($password),
                'role_id'		=> $this->input->post('role_id'),
                'is_active'		=> $this->input->post('is_active')
            );
        }

		/*** Simpan Data ke Database ***/
		$this->m_apps->updateData('user',$id,$data);
		redirect('user?msg_id=402');
	}
	
	public function cari(){
		$keyword = $this->input->get('keyword');
		
		$data = array(
                'Actions'       => $this->Actions,
                'SubActions'    => $this->SubAction,
                'num'		    => 0,
				'getData'	    => $this->m_user->cariData($keyword)
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/user/index');
		$this->load->view('elements/footer');
	}
	
	public function delete($id){
		$this->m_apps->delData('user',$id);
		redirect('user?msg_id=401');
	}
	
	public function is_existed(){
		$nama	= $this->input->post('nmlengkap');
		$data	= array(
				'getData'	=> $this->m_user->getbyName($nama)
		);
		
		$this->load->view('pages/user/ajax_existedata',$data);
	}

    public function test_unit(){
        $role = $this->m_user->arrRole();
    }
}