<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    function __construct(){
        session_start();
        parent::__construct();
        /* cek login */
        if (!isset($_SESSION['username'])){
            redirect('login');
        } else {
            /* cek user role */
            $this->role 	= $_SESSION['role'];
            $this->username = $_SESSION['username'];
            $this->Actions  = $this->m_actions->getParentActions($this->role);
            $this->SubAction= $this->m_actions->getSubActions($this->role);
        }
    }
	
	private function paging($table,$url,$per_page,$numlinks) {
		$jml		= $this->db->get($table);
	
		//pengaturan pagination
		$config['base_url'] 	= $url;
		$config['total_rows'] 	= $jml->num_rows();
		$config['num_links']	= $numlinks;
		$config['per_page'] 	= $per_page;
		$config['full_tag_open'] = "<ul class='pagination pagination-sm'>";
		$config['full_tag_close'] = '</ul>';
		$config['first_tag_open'] = '<li>';
		$config['first_page'] 	= 'Awal';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_page'] 	= 'Akhir';
		$config['last_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_page'] 	= 'Next';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_page'] 	= 'Prev';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class=active><a href=#>';
		$config['cur_tag_close'] = '</a></li>';
	
		//inisialisasi config
		$this->pagination->initialize($config);
	}
	
	public function index($id=0)
	{
		//pengaturan pagination
		$table 		= 'event';
		$url   		= base_url('event/index');
		$numlinks	= 2;
		$per_page	= 20;
		
		$this->paging($table, $url, $per_page, $numlinks);
		
		$data = array(
                'Actions'       => $this->Actions,
                'SubActions'    => $this->SubAction,
				'hal'		    => $this->pagination->create_links(),
				'num'		    => $id,
                'getShooter'    => $this->m_apps->getData('shooter'),
                'getRifle'	    => $this->m_apps->getData('rifle'),
                'getAmmo'       => $this->m_apps->getData('ammo'),
                'getKesatuan'   => $this->m_apps->getData('kesatuan'),
				//'getData'	    => $this->m_event->getData($per_page,$id)
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/report/index');
		$this->load->view('elements/footer');
	}

    public function summary(){
        $shooter_id = $this->input->get('shooter_id');
        $caliber    = $this->input->get('kaliber');
        $rifle      = $this->input->get('senapan');
        $stardate   = $this->input->get('startdate');
        $endate     = $this->input->get('endate');

        /***
        $dataScore = mysql_query("select score from review where shooter_name='$name' order by id asc");
        if (mysql_num_rows($dataScore) != 0){
            while ($r = mysql_fetch_array($dataScore, MYSQL_ASSOC)){
                $score[]= $r['score'];
            }
        }

        $jsonScore = json_encode($score,true);

        $dataAccu = mysql_query("select accurate from review where shooter_name='$name' order by id asc");
        if (mysql_num_rows($dataAccu) != 0){
            while ($a = mysql_fetch_array($dataAccu, MYSQL_ASSOC)){
                $accurate[]= $a['accurate']*10;
            }
        }

        $jsonAccurate = json_encode($accurate,true);
         ***/
        // JSON DATA

        $score = $this->m_report->getShooter($shooter_id,$caliber,$rifle,$stardate,$endate,2);
        if (count($score)){
            $jsonscore  = json_encode($score);
        } else {
            $jsonscore  = '';
        }
        $accurate = $this->m_report->getShooter($shooter_id,$caliber,$rifle,$stardate,$endate,3);
        if (count($accurate)){
            $jsonaccurate   = json_encode($accurate);
        } else {
            $jsonaccurate   = '';
        }

        //$jsonscore2     = json_encode($this->m_report->getShooter($name,$caliber,$rifle,$stardate,$endate,4));
        //$jsonaccurate2  = json_encode($this->m_report->getShooter($name,$caliber,$rifle,$stardate,$endate,5));

        $data = array(
            'Actions'       => $this->Actions,
            'SubActions'    => $this->SubAction,
            'getShooter'    => $this->m_shooter->getDatabyID($shooter_id),
            'getData'	    => $this->m_report->getShooter($shooter_id,$caliber,$rifle,$stardate,$endate,6),
            'getScore'      => $jsonscore,
            'getAcc'        => $jsonaccurate,
            //'getScore2'  => $jsonscore2,
            //'getAcc2'    => $jsonaccurate2
        );

        $this->load->view('elements/header',$data);
        $this->load->view('pages/report/summary');
        $this->load->view('elements/footer');
    }
	
	public function cari(){
		$keyword = $this->input->get('keyword');
		
		$data = array(
            'Actions'       => $this->Actions,
            'SubActions'    => $this->SubAction,
			'num'		    => 0,
			'getData'	    => $this->m_event->cariData($keyword)
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/event/cari');
		$this->load->view('elements/footer');
	}
	
	public function delete($id){
		$this->m_apps->delData('event',$id);
		redirect('event?msg_id=401');
	}

    public function testing(){
        /***
        //$sql = "select score from review";
        $q   = $this->db->get('review');
        foreach ($q->result() as $row){
            $score[] = $row->score;
        }
        /***
        $data = mysql_query("select score from review");
        if (mysql_num_rows($data) != 0){
            while ($r = mysql_fetch_array($data, MYSQL_ASSOC)){
                $score[]= $r['score'];
            }
        }
        ***/

        $name       = $this->input->get('name');
        $caliber    = $this->input->get('kaliber');
        $rifle      = $this->input->get('senapan');
        $stardate   = $this->input->get('startdate');
        $endate     = $this->input->get('endate');

        //getShooter($name,$caliber,$rifle,$startdate,$endate,$code)
        $score = $this->m_report->getShooter($name,$caliber,$rifle,$stardate,$endate,1);
        //$data = $this->m_report->getData($name,$caliber,$rifle,$stardate,$endate,1);
        echo json_encode($score,true);
    }

    public function demo($pdf,$id){
        //$id = $this->input->get('id');
        $data['pdf'] = base_url($pdf.'/'.$id);
        $this->load->view('pages/report/pdf',$data);
    }
}