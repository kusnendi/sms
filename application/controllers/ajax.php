<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		session_start();
		parent::__construct();
		/* cek login */
		if (!isset($_SESSION['username'])){
			redirect('login');
		} else {
			/* cek user role */
			$this->role 	= $_SESSION['role'];
			$this->username = $_SESSION['username'];
		}
	}
	
	public function listAmmo(){
		$data = array(
				'getAmmo'	=> $this->m_ammo->getbyRifle($this->input->post('rifle_id'))
		);
		$this->load->view('pages/event/ajax_list_ammo',$data);
	}
}