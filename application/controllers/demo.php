<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of demo
 *
 * @author nendi
 */
class Demo extends CI_Controller {
    //put your code here
    public function result_row(){
        $ip           = $this->input->post('ip');
        $result       = $this->m_match->getShooterbyIP($ip);
        
        if ($result == false)
        {
            echo 'Failed to find active match !';//
        }
        else
        {
            $image = $_FILES['image']['name'];
            $arrResult  = array(
                'match_id'  => $result->id,
                'nilai'     => $this->input->post('nilai'),
                'image'     => $image
            );
            //move uploaded image
            move_uploaded_file($_FILES['image']['tmp_name'], 'assets/match/'.$image);
            //save data to database
            $this->m_apps->simpanData('result',$arrResult);
            echo 'success';
        }
    }
    
    public function get(){
        $r = $this->m_match->getDemo();
        echo $r->shooter_id;
    }
    
    public function crypt(){
        $key  = '12345678abcdefgh';
        $text = 'Dev134719';
        $hash = trim(base64_encode(MCRYPT_ENCRYPT(MCRYPT_RIJNDAEL_256,$key,$text,MCRYPT_MODE_ECB,  mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
        
        echo $hash;
    }
    
    public function decrypt(){
        $key  = '12345678abcdefgh';
        $text = 'xkUlI7Kb08qQCpKHtcBicqeyemlz+i+Fuxj/YQ/rafA=';
        $hash = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB),MCRYPT_RAND)));
        
        echo $hash;
    }
}
