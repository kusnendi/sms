<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    function __construct(){
        session_start();
        parent::__construct();
        /* cek login */
        if (!isset($_SESSION['username'])){
            redirect('login');
        } else {
            /* cek user role */
            $this->role 	= $_SESSION['role'];
            $this->username = $_SESSION['username'];
            $this->Actions  = $this->m_actions->getParentActions($this->role);
            $this->SubAction= $this->m_actions->getSubActions($this->role);
        }
    }
	
	private function paging($table,$url,$per_page,$numlinks) {
		$jml		= $this->db->get($table);
	
		//pengaturan pagination
		$config['base_url'] 	= $url;
		$config['total_rows'] 	= $jml->num_rows();
		$config['num_links']	= $numlinks;
		$config['per_page'] 	= $per_page;
		$config['full_tag_open'] = "<ul class='pagination pagination-sm'>";
		$config['full_tag_close'] = '</ul>';
		$config['first_tag_open'] = '<li>';
		$config['first_page'] 	= 'Awal';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_page'] 	= 'Akhir';
		$config['last_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_page'] 	= 'Next';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_page'] 	= 'Prev';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class=active><a href=#>';
		$config['cur_tag_close'] = '</a></li>';
	
		//inisialisasi config
		$this->pagination->initialize($config);
	}
	
	public function index($id=0)
	{
		//pengaturan pagination
		$table 		= 'event';
		$url   		= base_url('event/index');
		$numlinks	= 2;
		$per_page	= 10;
		
		$this->paging($table, $url, $per_page, $numlinks);

        /*** filter event by role ***/
        if ($this->role == 0){
            $getEvent = $this->m_event->getData($per_page,$id);
        } else {
            $getEvent = $this->m_event->getbyUserID($_SESSION['user_id'],$per_page,$id);
        }

		$data = array(
            'Actions'       => $this->Actions,
            'SubActions'    => $this->SubAction,
			'hal'		    => $this->pagination->create_links(),
			'num'		    => $id,
			'getData'	    => $getEvent
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/event/index');
		$this->load->view('elements/footer');
	}
	
	public function view($id){
		
		$data = array(
            'Actions'       => $this->Actions,
            'SubActions'    => $this->SubAction,
			'getData'	    => $this->m_event->getDatabyID($id),
            'getShooter'    => $this->m_match->getShooter($id)
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/event/view');
		$this->load->view('elements/footer');
	}

    public function summary($match_id,$id){

        $data = array(
            'Actions'       => $this->Actions,
            'SubActions'    => $this->SubAction,
            'getData'	    => $this->m_event->getDatabyID($id),
            'getShooter'    => $this->m_match->getShooterMatchID($match_id),
            'getSReal'      => $this->m_result->getData($match_id),
            'getWarming'    => $this->m_warming->getData($match_id)
        );

        $this->load->view('elements/header',$data);
        $this->load->view('pages/event/summary');
        $this->load->view('elements/footer');
    }
	
	public function add(){
		$data = array(
            'Actions'       => $this->Actions,
            'SubActions'    => $this->SubAction,
			'getRifle'	    => $this->m_apps->getData('rifle'),
			'getRange'	    => $this->m_apps->getData('range'),
            'getAmmo'	    => $this->m_apps->getData('ammo')
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/event/tambah');
		$this->load->view('elements/footer');
	}
	
	public function edit($id){
		$data = array(
                'getRifle'	=> $this->m_apps->getData('rifle'),
                'getRange'	=> $this->m_apps->getData('range'),
                'getAmmo'   => $this->m_apps->getData('ammo'),
                'getData'	=> $this->m_event->getDatabyID($id)
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/event/edit');
		$this->load->view('elements/footer');
	}
	
	function simpan(){
        /*** cek role id ***/
		$data = array(
				'judul'	    => $this->input->post('judul'),
				'rifle_id'  => $this->input->post('rifle_id'),
				'ammo_id'	=> $this->input->post('ammo_id'),
				'range_id'	=> $this->input->post('range_id'),
				'max_shots'	=> $this->input->post('max_shots'),
				'deskripsi'	=> $this->input->post('deskripsi'),
                'user_id'   => $_SESSION['user_id']
		);
		/*** Simpan Data ke Database ***/
		$this->m_apps->simpanData('event',$data);
		redirect('event?msg_id=402');
	}
	
	function update(){
        //id for update condition
        $id = $this->input->post('id');
        $data = array(
            'judul'	    => $this->input->post('judul'),
            'rifle_id'  => $this->input->post('rifle_id'),
            'ammo_id'	=> $this->input->post('ammo_id'),
            'range_id'	=> $this->input->post('range_id'),
            'max_shots'	=> $this->input->post('max_shots'),
            'deskripsi'	=> $this->input->post('deskripsi')
        );
		/*** Simpan Data ke Database ***/
		$this->m_apps->updateData('event',$id,$data);
		redirect('event?msg_id=402');
	}
	
	public function cari(){
		$keyword = $this->input->get('keyword');
		
		$data = array(
            'Actions'       => $this->Actions,
            'SubActions'    => $this->SubAction,
			'num'		    => 0,
			'getData'	    => $this->m_event->cariData($keyword)
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/event/index');
		$this->load->view('elements/footer');
	}
	
	public function delete($id){
		$this->m_apps->delData('event',$id);
		redirect('event?msg_id=401');
	}
}