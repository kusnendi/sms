<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kesatuan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    function __construct(){
        session_start();
        parent::__construct();
        /* cek login */
        if (!isset($_SESSION['username'])){
            redirect('login');
        } else {
            /* cek user role */
            $this->role 	= $_SESSION['role'];
            $this->username = $_SESSION['username'];
            $this->Actions  = $this->m_actions->getParentActions($this->role);
            $this->SubAction= $this->m_actions->getSubActions($this->role);
        }
    }
	
	private function paging($table,$url,$per_page,$numlinks) {
		$jml		= $this->db->get($table);
	
		//pengaturan pagination
		$config['base_url'] 	= $url;
		$config['total_rows'] 	= $jml->num_rows();
		$config['num_links']	= $numlinks;
		$config['per_page'] 	= $per_page;
		$config['full_tag_open'] = "<ul class='pagination pagination-sm'>";
		$config['full_tag_close'] = '</ul>';
		$config['first_tag_open'] = '<li>';
		$config['first_page'] 	= 'Awal';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_page'] 	= 'Akhir';
		$config['last_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_page'] 	= 'Next';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_page'] 	= 'Prev';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class=active><a href=#>';
		$config['cur_tag_close'] = '</a></li>';
	
		//inisialisasi config
		$this->pagination->initialize($config);
	}
	
	public function index($id=0)
	{
		//pengaturan pagination
		$table 		= 'kesatuan';
		$url   		= base_url('kesatuan/index');
		$numlinks	= 2;
		$per_page	= 20;
		
		$this->paging($table, $url, $per_page, $numlinks);
		
		$data = array(
            'Actions'       => $this->Actions,
            'SubActions'    => $this->SubAction,
				'hal'		=> $this->pagination->create_links(),
				'num'		=> $id,
				'getData'	=> $this->m_kesatuan->getData($per_page,$id)
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/kesatuan/index');
		$this->load->view('elements/footer');
	}
	
	public function detail($id){
		
		$data = array(
            'Actions'       => $this->Actions,
            'SubActions'    => $this->SubAction,
				'getData'	=> $this->m_kesatuan->getDatabyID($id)
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/kesatuan/detail');
		$this->load->view('elements/footer');
	}
	
	public function add(){
        $data = array(
            'Actions'       => $this->Actions,
            'SubActions'    => $this->SubAction
        );
		$this->load->view('elements/header',$data);
		$this->load->view('pages/kesatuan/tambah');
		$this->load->view('elements/footer');
	}
	
	public function edit($id){
		$data = array(
            'Actions'       => $this->Actions,
            'SubActions'    => $this->SubAction,
				'getData'	=> $this->m_kesatuan->getDatabyID($id)
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/kesatuan/edit');
		$this->load->view('elements/footer');
	}
	
	function simpan(){
		$data = array(
				'nama'      => $this->input->post('nama'),
				'alamat'	=> $this->input->post('alamat')
		);
		/*** Simpan Data ke Database ***/
		$this->m_apps->simpanData('kesatuan',$data);
		redirect('kesatuan?msg_id=402');
	}
	
	function update(){
        $id   = $this->input->post('id');
		$data = array(
            'nama'      => $this->input->post('nama'),
            'alamat'	=> $this->input->post('alamat')
		);
		/*** Simpan Data ke Database ***/
		$this->m_apps->updateData('kesatuan',$id,$data);
		redirect('kesatuan?msg_id=402');
	}
	
	public function cari(){
		$keyword = $this->input->get('keyword');
		
		$data = array(
            'Actions'       => $this->Actions,
            'SubActions'    => $this->SubAction,
				'num'		=> 0,
				'getData'	=> $this->m_kesatuan->cariData($keyword)
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/kesatuan/cari');
		$this->load->view('elements/footer');
	}
	
	public function delete($id){
		$this->m_apps->delData('kesatuan',$id);
		redirect('dashboard?msg_id=401');
	}
}