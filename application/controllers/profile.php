<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    function __construct(){
        session_start();
        parent::__construct();
        /* cek login */
        if (!isset($_SESSION['username'])){
            redirect('login');
        } else {
            /* cek user role */
            $this->role 	= $_SESSION['role'];
            $this->username = $_SESSION['username'];
            $this->uid      = $_SESSION['user_id'];
            $this->Actions  = $this->m_actions->getParentActions($this->role);
            $this->SubAction= $this->m_actions->getSubActions($this->role);
        }
    }
	
	public function index()
	{
		$data = array(
                'Actions'       => $this->Actions,
                'SubActions'    => $this->SubAction,
                'getData'	    => $this->m_shooter->getbyUserID($this->uid),
                'Kesatuan'      => $this->m_apps->getData('kesatuan')
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/profile/profile');
		$this->load->view('elements/footer');
	}
	
	public function detail($id){
		
		$data = array(
            'Actions'       => $this->Actions,
            'SubActions'    => $this->SubAction,
            'Kesatuan'      => $this->m_apps->getData('kesatuan'),
            'getData'	    => $this->m_profile->getDatabyID($id)
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/profile/detail');
		$this->load->view('elements/footer');
	}
	
	public function add(){
		$data = array(
                'Actions'       => $this->Actions,
                'SubActions'    => $this->SubAction,
				'getKesatuan'	=> $this->m_apps->getData('kesatuan')
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/profile/tambah');
		$this->load->view('elements/footer');
	}
	
	public function edit($id){
		$data = array(
                'Actions'       => $this->Actions,
                'SubActions'    => $this->SubAction,
                'getData'	    => $this->m_profile->getDatabyID($id),
                'getKesatuan'   => $this->m_apps->getData('kesatuan')
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/profile/edit');
		$this->load->view('elements/footer');
	}
	
	public function simpan(){
		
		$data = array(
                'user_id'       => $this->input->post('user_id'),
				'nama_lengkap'	=> $this->input->post('nama'),
				'kesatuan_id'	=> $this->input->post('kesatuan_id'),
				'email'			=> $this->input->post('email'),
				'handphone'		=> $this->input->post('handphone'),
				'alamat'		=> $this->input->post('alamat')
		);
		/*** Simpan Data ke Database ***/
		$this->m_apps->simpanData('shooter',$data);
		redirect('profile?msg_id=402');
	}
	
	public function update($id){
				
		$data = array(
				'nama_lengkap'	=> $this->input->post('nama'),
				'kesatuan_id'	=> $this->input->post('kesatuan_id'),
				'email'			=> $this->input->post('email'),
				'handphone'		=> $this->input->post('handphone'),
				'alamat'		=> $this->input->post('alamat')
		);
		/*** Simpan Data ke Database ***/
		$this->m_apps->updateData('shooter',$id,$data);
		redirect('profile?msg_id=402');
	}
	
	public function cari(){
		$keyword = $this->input->get('keyword');
		
		$data = array(
                'Actions'       => $this->Actions,
                'SubActions'    => $this->SubAction,
				'num'		    => 0,
				'getData'	    => $this->m_profile->cariData($keyword)
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/profile/index');
		$this->load->view('elements/footer');
	}
	
	public function delete($id){
		$this->m_apps->delData('profile',$id);
		redirect('profile?msg_id=401');
	}
	
	public function ajax_profil(){
		$id = $this->input->post('id');
		
		$data = array(
				'getData'	=> $this->m_profile->getDatabyID($id)
		);
		
		$this->load->view('pages/profile/ajax_profil',$data);
	}
	
	public function is_existed(){
		$nama	= $this->input->post('nmlengkap');
		$data	= array(
				'getData'	=> $this->m_profile->getbyName($nama)
		);
		
		$this->load->view('pages/profile/ajax_existedata',$data);
	}
}