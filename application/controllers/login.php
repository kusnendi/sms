<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct(){
        session_start();
        parent::__construct();
    }

	function index() {
        if (isset($_SESSION['username'])){
            if ($_SESSION['role'] == 2){
                redirect('profile');
            }
            redirect('dashboard');
        }
		$this->load->view('pages/login/index');
	}
	
	function user_auth() {
		$username 	= $this->input->post('username');
		$password 	= md5($this->input->post('password'));
		$manualSQL	= mysql_query("SELECT * FROM user WHERE username='$username' AND password='$password' AND is_active=1");
		$query 		= $this->m_login->cek_user($username,$password);
		
		if ($query){
			$login = mysql_fetch_array($manualSQL);
			
			// Mulai Session Baru
			session_start();

            $_SESSION['user_id'] 		= $login['id'];
            $_SESSION['username'] 		= $login['username'];
			$_SESSION['password'] 		= $login['password'];
			$_SESSION['fullname'] 		= $login['fullname'];
			$_SESSION['role']	   		= $login['role_id'];
            $_SESSION['display']   		= $login['display_picture'];
			
			$sid_lama = session_id();
			session_regenerate_id();
			$sid_baru = session_id();
			
			mysql_query("update user set sid='$sid_baru' WHERE username='$username'");
            if ($_SESSION['role'] == 2){
                redirect('profile');
            }
			redirect('dashboard');
		}
		else {
			redirect('login?msg=failed');
		}
	}
	
	//user logout
	function destroy(){
		// Mulai Session
		session_start();
		// Keluar dari Portal
		session_destroy();
		// Redirect ke halaman utama
		redirect('login');
	}
}
