<?php
require APPPATH.'/libraries/REST_Controller.php';

class Api extends REST_Controller {

    public function index_get(){
        //$data = array('returned: '. $this->get('id'));
        if (!$this->get('id')){
            $this->response(NULL, 400);
        }
        $users = array(
            1   => array('name' => 'nendi', 'email' => 'nendi@ace.co.id'),
            2   => array('name' => 'husni', 'email' => 'husni@ace.co.id')
        );
        
        $user = $users[$this->get('id')];
        
        if ($user){
            $this->response($user,200);
        } else {
            $this->response(array('error' => 'User could not be found'), 404);
        }
        //$data = array('returned: '. $this->post('id'));
        //$this->response($data);
    }
    
    public function index_post(){
        //get shooter camera ip
        $ip           = $this->input->post('ip');
        //cek ip and active match
        $result       = $this->m_match->getShooterbyIP($ip);
        
        if ($result == false)
        {
            $this->response(array('error' => 'User could not be found'), 404);
        }
        else
        {
            $image = $_FILES['image']['name'];
            $arrResult  = array(
                'match_id'  => $result->id,
                'nilai'     => $this->input->post('nilai'),
                'image'     => $image
            );
            //move uploaded image
            move_uploaded_file($_FILES['image']['tmp_name'], 'assets/match/'.$image);
            //save data to database
            $this->m_apps->simpanData('result',$arrResult);
            $this->response($arrResult,200);
        }
        /***
        if ($this->post('ip') == '192.168.1.1'){
            $file = $_FILES['image']['name'];
            $arrUser = array(
                'username'          => $this->post('username'),
                'password'          => md5($this->post('password')),
                'display_picture'   => $file
            );
            move_uploaded_file($_FILES['image']['tmp_name'], 'assets/'.$file);
            $this->m_apps->simpanData('user',$arrUser);
            $this->response(array('message' => 'Success, data have inserted into table'),200);
        } else {
            $this->response(array('error' => 'User could not be found'), 404);
        }
        ***/
    }
}