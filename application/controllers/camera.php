<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of camera
 *
 * @author nendi
 */
class Camera extends CI_Controller{
    
    function __construct() {
        session_start();
        parent::__construct();
         /* cek login */
        if (!isset($_SESSION['username'])){
            redirect('login');
        } else {
            /* cek user role */
            $this->role     = $_SESSION['role'];
            $this->username = $_SESSION['username'];
            $this->Actions  = $this->m_actions->getParentActions($this->role);
            $this->SubAction= $this->m_actions->getSubActions($this->role);
        }
    }
    
    //index master camera
    public function index(){
        $data = array(
            'Actions'       => $this->Actions,
            'SubActions'    => $this->SubAction,
            'getData'
        );
        
        //make view index camera
        $this->load->view('elements/header',$data);
        $this->load->view('pages/camera/index');
        $this->load->view('elements/footer');
    }
     
    //add new camera
    public function create(){
        
    }

    //save data to database
    public function save(){
        
    }
    
    //edit existing camera
    public function edit($id){
        
    }
    
    //update camera to database
    public function update(){
        
    }
    
    //delete camera from database
    public function delete($id){
        
    }
}