<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		session_start();
		parent::__construct();
		/* cek login */
		if (!isset($_SESSION['username'])){
			redirect('login');
		} else {
			/* cek user role */
			$this->role 	= $_SESSION['role'];
			$this->username = $_SESSION['username'];
            $this->Actions  = $this->m_actions->getParentActions($this->role);
            $this->SubAction= $this->m_actions->getSubActions($this->role);
		}
	}
	
	private function paging($table,$url,$per_page,$numlinks) {
		$jml		= $this->db->get($table);
	
		//pengaturan pagination
		$config['base_url'] 	= $url;
		$config['total_rows'] 	= $jml->num_rows();
		$config['num_links']	= $numlinks;
		$config['per_page'] 	= $per_page;
		$config['full_tag_open'] = "<ul class='pagination pagination-sm'>";
		$config['full_tag_close'] = '</ul>';
		$config['first_tag_open'] = '<li>';
		$config['first_page'] 	= 'Awal';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_page'] 	= 'Akhir';
		$config['last_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_page'] 	= 'Next';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_page'] 	= 'Prev';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class=active><a href=#>';
		$config['cur_tag_close'] = '</a></li>';
	
		//inisialisasi config
		$this->pagination->initialize($config);
	}
	
	public function index($id=NULL)
	{
		$jml_shooter = $this->db->count_all('shooter');
        $jml_event   = $this->db->count_all('event');
        $jml_kesatuan= $this->db->count_all('kesatuan');
        $jml_rifle   = $this->db->count_all('rifle');

        $data = array(
            'Actions'       => $this->Actions,
            'SubActions'    => $this->SubAction,
            'jml_shooter'   => $jml_shooter,
            'jml_event'     => $jml_event,
            'jml_kesatuan'  => $jml_kesatuan,
            'jml_rifle'     => $jml_rifle
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/dashboard/index');
		$this->load->view('elements/footer');
	}
	
	public function detail($id){
		
		$data = array(
				'getData'	=> $this->m_kartu_nama->getDatabyID($id)
		);
		
		$this->load->view('elements/header',$data);
		$this->load->view('pages/dashboard/detail');
		$this->load->view('elements/footer');
	}
	
	public function json(){
		$data = $this->m_kartu_nama->getDatabyID(6);
		
		echo json_encode($data);
	}

    public function unit_test(){
        echo $_SESSION['display'];
    }

}