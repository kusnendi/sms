<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
    <?php
    if ($_SESSION['role'] != 2){
        ?>
        <li class="active">
            <a href="<?php echo site_url('dashboard')?>">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
        </li>
        <?php
    }
    ?>

    <?php
    if (count($Actions)){
        foreach ($Actions as $a){
            if ($a->no_parent == 1){
                ?>
                <li class="">
                    <a href="<?php echo site_url($a->controller)?>">
                        <i class="<?php echo $a->icon ?>"></i> <span><?php echo $a->menu ?></span>
                    </a>
                </li>
                <?php
            } else {
                ?>
                <li class="treeview">
                    <a href="#">
                        <i class="<?php echo $a->icon ?>"></i> <span><?php echo $a->menu ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php
                        if (count($SubActions)){
                            foreach ($SubActions as $sa){
                                if ($sa->parent == $a->id){
                                ?>
                                <li><a href="<?php echo site_url($sa->controller)?>"><i class="fa fa-angle-double-right"></i> <?php echo $sa->menu ?></a></li>
                                <?php
                                }
                            }
                        }
                        ?>
                    </ul>
                </li>
                <?php
            }
        }
    }
    ?>
</ul>