<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>Summary</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Report</a></li>
    </ol>
</section>

<?php
if (count($getData) > 0){
    ?>
    <!-- Main content -->
    <section class="content">
        <!-- Event Deskription -->
        <div class="row">
            <div class="col-md-6">
                <div>
                    <canvas id="canvas"></canvas>
                </div>
            </div>

            <div class="col-md-6">
                <div class="box box-solid box-primary">
                    <div class="box-header">
                        <h3 class="box-title">User Information</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-primary btn-sm" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <?php
                            if (count($getShooter) > 0){
                            foreach ($getShooter as $s){
                            ?>
                            <div class="col-md-4">
                                <img class="img-responsive" src="<?php echo base_url('assets/img/users/'.$s->picture) ?>">
                            </div>
                            <div class="col-md-8">
                                <table class="table">
                                    <tr>
                                        <th>Nama</th>
                                        <td><?php echo $s->nama_lengkap?></td>
                                    </tr>
                                    <tr>
                                        <th>Handphone</th>
                                        <td><?php echo $s->handphone?></td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td><?php echo $s->email?></td>
                                    </tr>
                                    <tr>
                                        <th>Alamat</th>
                                        <td><?php echo $s->alamat?></td>
                                    </tr>
                                    <tr>
                                        <th>Kesatuan</th>
                                        <td><?php echo $s->kesatuan?></td>
                                    </tr>
                                </table>
                            </div>
                            <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- row 1-->

        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped">
                    <tr>
                        <th>#</th>
                        <th>Tanggal</th>
                        <th>Event</th>
                        <th>Max Shots</th>
                        <th>Score</th>
                        <th>Accuracy (%)</th>
                    </tr>
                    <?php
                    $i=1;
                    foreach ($getData as $data){
                        ?>
                        <tr>
                            <td><?php echo $i ?></td>
                            <td><?php echo $data->create?></td>
                            <td><?php echo $data->judul?></td>
                            <td><?php echo $data->max_shots?></td>
                            <td><?php echo $data->score?></td>
                            <td><?php echo $data->accurate?></td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </table>
            </div>
        </div><!-- row 2-->
    </section><!-- /.content -->
    <?php
} else {
    ?>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
               <div class="well well-sm">
                   <p>Tidak ada data yang di temukan !</p>
               </div>
               <button class="btn btn-danger btn-flat" onclick="self.history.back();"><i class="fa fa-angle-left"></i> Kembali</button>
            </div>
        </div>
    </section>
    <?php
}
?>

<script>

    var barChartData = {
        labels : ["Event 1","Event 2","Event 3"],
        datasets : [
            /***
            {
                fillColor : "rgba(240,239,136,0.5)",
                strokeColor : "rgba(240,239,136,0.8)",
                highlightFill : "rgba(240,239,136,0.75)",
                highlightStroke : "rgba(240,239,136,1)",
                data : [0,20,50]
            },
            {
                fillColor : "rgba(191,84,46,0.5)",
                strokeColor : "rgba(191,84,46,0.8)",
                highlightFill : "rgba(191,84,46,0.75)",
                highlightStroke : "rgba(191,84,46,1)",
                data : [0,40,100]
            },
             ***/
            {
                fillColor : "rgba(220,220,220,0.5)",
                strokeColor : "rgba(220,220,220,0.8)",
                highlightFill: "rgba(220,220,220,0.75)",
                highlightStroke: "rgba(220,220,220,1)",
                data : <?php echo $getScore ?>
            },
            {
                fillColor : "rgba(151,187,205,0.5)",
                strokeColor : "rgba(151,187,205,0.8)",
                highlightFill : "rgba(151,187,205,0.75)",
                highlightStroke : "rgba(151,187,205,1)",
                data : <?php echo $getAcc ?>
            }
        ]

    }
    window.onload = function(){
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx).Bar(barChartData, {
            responsive : true
        });
    }

</script>