<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Report</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Report</a></li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<!-- Kotak Pencarian -->
	<div class="callout callout-info">
		<form class="form" method="get" action="<?php echo site_url('report/summary') ?>">
			<div class="row form-group">
				<div class="col-md-6">
                    <label>Shooter</label>
                    <?php
                    if ($_SESSION['role'] == 0){
                        ?>
                        <select data-placeholder="Pilih Shooter..." name="shooter_id" class="chosen-select form-control" style="width:510px;">
                            <option value=""></option>
                            <?php
                            if (count($getShooter) > 0){
                                foreach ($getShooter as $user){
                                    ?>
                                    <option value="<?php echo $user->id ?>"><?php echo $user->nama_lengkap ?></option>
                                <?php
                                }
                            }
                            ?>
                        </select>
                        <?php
                    } else {
                        ?>
                        <select data-placeholder="Pilih Shooter..." name="shooter_id" class="chosen-select form-control" style="width:510px;">
                            <option value=""></option>
                            <?php
                            if (count($getShooter)){
                                foreach ($getShooter as $user){
                                    if ($user->user_id == $_SESSION['user_id']){
                                        ?>
                                        <option value="<?php echo $user->id ?>" selected><?php echo $user->nama_lengkap ?></option>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </select>
                        <?php
                    }
                    ?>
				</div>
				<div class="col-md-6">
                    <label>Kesatuan</label>
                    <select name="kesatuan" class="chosen-select form-control">
                        <option selected>Any</option>
                        <?php
                        if (count($getKesatuan) > 0){
                            foreach ($getKesatuan as $k){
                                ?>
                                <option value="<?php echo $k->id ?>"><?php echo $k->nama ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
				</div>
			</div>
            <div class="row form-group">
                <div class="col-md-6">
                    <label>Senapan</label>
                    <select name="senapan" class="chosen-select form-control">
                        <option selected>Any</option>
                        <?php
                        if (count($getRifle) > 0){
                            foreach ($getRifle as $r){
                                ?>
                                <option value="<?php echo $r->id ?>"><?php echo $r->name ?></option>
                            <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label>Kaliber</label>
                    <select name="kaliber" class="chosen-select form-control">
                        <option selected>Any</option>
                        <?php
                        if (count($getAmmo) > 0){
                            foreach ($getAmmo as $a){
                                ?>
                                <option value="<?php echo $a->id ?>"><?php echo $a->type ?></option>
                            <?php
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-6">
                    <label>Start Date</label>
                    <input class="form-control" name="startdate" type="date">
                </div>
                <div class="col-md-6">
                    <label>End Date</label>
                    <input class="form-control" type="date" name="endate">
                </div>
            </div>
			<button class="btn btn-danger btn-flat" type="submit"><i class="fa fa-search"></i>&nbsp;&nbsp;Cari</button>
		</form>
	</div>

	<?php //$this->load->view('pages/event/data')?>

</section><!-- /.content -->