<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Edit Event</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Event</a></li>
	</ol>
</section> <!-- /content-header -->
<?php
if (count($getData) > 0){
    foreach ($getData as $data){
    ?>
<!-- Main content -->
<section class="content">
	<form role="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('event/update')?>">
        <input type="hidden" name="id" value="<?php echo $data->id ?>">
		<h4>Judul</h4>
		<div class="row form-group">
			<div class="col-md-8">
	          	<input id="judul" class="form-control" type="text" name="judul" value="<?php echo $data->judul ?>" placeholder="Nama Event *" required />
			</div>
		</div>
		<h4>Preference</h4>
		<div class="row form-group">
			<div class="col-md-4">
				<select id="rifle_id" name="rifle_id" class="form-control">
					<option value="">Senapan</option>
					<?php 
					if (count($getRifle) > 0){
						foreach ($getRifle as $r){
							if ($r->id == $data->rifle_id){
                                ?>
                                <option value="<?php echo $r->id?>" selected><?php echo $r->senapan?></option>
                                <?php
                            }
                            else {
                                ?>
                                <option value="<?php echo $r->id?>"><?php echo $r->senapan?></option>
                                <?php
                            }
						}
					}
					?>
				</select>	
			</div>
			<div class="col-md-4">
				<select id="ammo_id" name="ammo_id" class="form-control">
					<option value=''>Peluru</option>
                    <?php
                    if (count($getAmmo) > 0){
                        foreach ($getAmmo as $a){
                            if ($a->id == $data->ammo_id){
                                ?>
                                <option value="<?php echo $a->id?>" selected><?php echo $a->jenis?></option>
                            <?php
                            }
                            else {
                                ?>
                                <option value="<?php echo $a->id?>"><?php echo $a->jenis?></option>
                            <?php
                            }
                        }
                    }
                    ?>
				</select>	
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-4">
				<select id="range_id" name="range_id" class="form-control">
					<option value=''>Jarak</option>
					<?php 
					if (count($getRange) > 0){
						foreach ($getRange as $j){
                            if ($j->id == $data->range_id){
                                ?>
                                <option value="<?php echo $j->id?>" selected><?php echo $j->jarak?> m</option>
                                <?php
                            }
                            else {
                                ?>
                                <option value="<?php echo $j->id?>"><?php echo $j->jarak?> m</option>
                                <?php
                            }
						}
					}
					?>
				</select>	
			</div>
			<div class="col-md-4">
				<input class="form-control" type="text" name="max_shots" value="<?php echo $data->max_shots ?>" placeholder="Max Shots">
			</div>
		</div>
		<h4>Deskripsi</h4>
		<div class="row form-group">
			<div class="col-md-8">
				<textarea class="form-control" name="deskripsi" placeholder="Detail deskripsi about this event"><?php echo $data->deskripsi ?></textarea>
			</div>
		</div>
		<div class="pull-left">
			<button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i>&nbsp;Simpan</button>
			<button class="btn btn-danger btn-flat" onclick="self.history.back();" type="button"><i class="fa fa-times"></i>&nbsp;Batal</button>
		</div>
	</form>
</section><!-- /.content -->
    <?php
    }
}
?>
					
<script type="text/javascript">
	$(document).ready(function(){
		$("#rifle_id").change(function(){
			loadListAmmo();
		})
	})
	
	
	function loadListAmmo()
	{
		var rifle_id = $("#rifle_id").val();
		$.ajax({
			type: "POST",
			url : "<?php echo site_url('ajax/listAmmo');?>",
			data: "rifle_id="+rifle_id,
			cache:false,
			success: function(data){
				//alert(data);return;
				$("select[name='ammo_id']").html(data);
				//document.add.frm.disabled=false;
			}
		})
	}
</script>