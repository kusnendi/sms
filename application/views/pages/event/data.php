	<!-- User Data -->
	<?php 
	if (count($getData) != 0){
	?>
	<div style="margin-top: 5px;" class="table-responsive">
        <?php
        if ($_SESSION['role'] == 2){
            ?>
            <table class="table table-hover" style="font-size: 13px; border-bottom: 1px solid #d9d9d9">
                <tr>
                    <th>#</th>
                    <th>Event</th>
                    <th>Keterangan</th>
                    <th></th>
                </tr>
                <?php
                $i = $num+1;
                foreach ($getData as $r){
                    ?>
                    <tr>
                        <td><?php echo $i?></td>
                        <td><?php echo $r->judul?></td>
                        <td><?php echo $r->deskripsi?></td>
                        <td>
                            <div class="pull-right">
                                <a href="<?php echo site_url('event/view/'.$r->id)?>"><i class="glyphicon glyphicon-list-alt"></i></a>&nbsp;
                            </div>
                        </td>
                    </tr>
                    <?php
                    $i++;
                }
                ?>
            </table>
            <?php
        } else {
            ?>
            <table class="table table-hover" style="font-size: 13px; border-bottom: 1px solid #d9d9d9">
                <tr>
                    <th>#</th>
                    <th>Event</th>
                    <th>Keterangan</th>
                    <th>Shooter</th>
                    <th></th>
                </tr>
                <?php
                $i = $num+1;
                foreach ($getData as $r){
                    ?>
                    <tr>
                        <td><?php echo $i?></td>
                        <td><?php echo $r->judul?></td>
                        <td><?php echo $r->deskripsi?></td>
                        <td><?php echo $r->shooter?></td>
                        <td>
                            <div class="pull-right">
                                <a href="<?php echo site_url('event/view/'.$r->id)?>"><i class="glyphicon glyphicon-list-alt"></i></a>&nbsp;
                                <a href="<?php echo site_url('event/delete/'.$r->id)?>" title="delete" onclick="return confirm('Anda yakin?');"><i class="glyphicon glyphicon-remove"></i></a>&nbsp;
                                <a href="<?php echo site_url('event/edit/'.$r->id)?>" title="edit"><i class="glyphicon glyphicon-check"></i></a>
                            </div>
                        </td>
                    </tr>
                    <?php
                    $i++;
                }
                ?>
            </table>
            <?php
        }
        echo $hal;
        ?>
	</div>
	<?php 
	}
	?>