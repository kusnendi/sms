<?php
if (count($getData) > 0){
    foreach ($getData as $data){
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?php echo $data->judul ?></h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Event</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Pesan Notifikasi -->
            <?php
            if (@$_GET['msg_id'] == 401){
                ?>
                <div class="callout callout-danger">
                    <h4>Hi, <?php echo $_SESSION['fullname']?>!</h4>
                    <p>Data berhasil di hapus, data yang telah di hapus tidak bisa di restore.</p>
                </div>
            <?php
            } elseif (@$_GET['msg_id'] == 402) {
                ?>
                <div class="callout callout-info">
                    <h4>Hi, <?php echo $_SESSION['fullname']?>!</h4>
                    <p>Data berhasil di simpan, lihat pada daftar data.</p>
                </div>
            <?php
            } elseif (@$_GET['msg_id'] == 405){
                ?>
                <div class="callout callout-info">
                    <h4>Hi, <?php echo $_SESSION['fullname']?>!</h4>
                    <p>Selamat datang kembali di ACE - Bussiness Card Management.</p>
                </div>
            <?php
            }
            ?>

            <!-- Event Deskription -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-solid box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Event Detail</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-primary btn-sm" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <table class="table table-condensed">
                                <tr>
                                    <td class="col-md-2">Senapan</td>
                                    <td> <?php echo $data->senapan ?> </td>
                                </tr>
                                <tr>
                                    <td>Kaliber</td>
                                    <td> <?php echo $data->ammo ?> </td>
                                </tr>
                                <tr>
                                    <td>Jarak</td>
                                    <td> <?php echo $data->jarak ?> </td>
                                </tr>
                                <tr>
                                    <td>Max. Shots</td>
                                    <td> <?php echo $data->max_shots ?> </td>
                                </tr>
                                <tr>
                                    <td>Deskripsi</td>
                                    <td> <?php echo $data->deskripsi ?> </td>
                                </tr>
                                <tr>
                                    <td>Tanggal</td>
                                    <td> <?php echo $data->create ?> </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Add Button -->
            <a href="<?php echo site_url('event/add')?>" class="btn btn-primary btn-flat pull-right" style="margin-bottom:5px;"><i class="fa fa-plus-square"></i>&nbsp;Tambah Shooter</a>
            <!-- Table Shooter List-->
            <div class="row">
                <div class="col-md-12">
                    <?php
                    if (count($getShooter) != 0){
                        ?>
                        <div style="margin-top: 5px;" class="table-responsive">
                            <table class="table table-hover" style="font-size: 13px; border-bottom: 1px solid #d9d9d9">
                                <tr>
                                    <th>#</th>
                                    <th>Shooter</th>
                                    <th>Kesatuan</th>
                                    <th>Score</th>
                                    <th>Accuracy (%)</th>
                                    <th></th>
                                </tr>
                                <?php
                                $i = 1;
                                foreach ($getShooter as $r){
                                    ?>
                                    <tr>
                                        <td><?php echo $i?></td>
                                        <td>
                                            <div class="user-panel">
                                                <div class="pull-left image">
                                                    <img src="<?php echo base_url('assets/img/users/'.$r->picture) ?>">
                                                </div>
                                                <div class="pull-left info">
                                                    <p><?php echo $r->nama_lengkap?></p>
                                                </div>
                                            </div>
                                        </td>
                                        <td><?php echo $r->kesatuan?></td>
                                        <td>W : <?php echo $r->score2?><br>R &nbsp;: <?php echo $r->score?></td>
                                        <td>W : <?php echo $r->accurate2?><br>R &nbsp;: <?php echo $r->accurate?></td>
                                        <td>
                                            <div class="pull-right">
                                                <a href="<?php echo site_url('event/summary/'.$r->id.'/'.$r->event_id)?>"><i class="glyphicon glyphicon-list-alt"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                            </table>
                        </div>
                    <?php
                    }
                    ?>
                    <a class="btn btn-primary" href="<?php echo site_url('event') ?>">Kembali</a>
                </div>
            </div>
        </section><!-- /.content -->
        <?php
    }
}
?>