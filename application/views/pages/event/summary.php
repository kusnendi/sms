<?php
if (count($getData) > 0){
    foreach ($getData as $data){
        ?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Summary<small><?php echo $data->judul ?></small></h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Event</a></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Pesan Notifikasi -->
            <?php
            if (@$_GET['msg_id'] == 401){
                ?>
                <div class="callout callout-danger">
                    <h4>Hi, <?php echo $_SESSION['fullname']?>!</h4>
                    <p>Data berhasil di hapus, data yang telah di hapus tidak bisa di restore.</p>
                </div>
            <?php
            } elseif (@$_GET['msg_id'] == 402) {
                ?>
                <div class="callout callout-info">
                    <h4>Hi, <?php echo $_SESSION['fullname']?>!</h4>
                    <p>Data berhasil di simpan, lihat pada daftar data.</p>
                </div>
            <?php
            } elseif (@$_GET['msg_id'] == 405){
                ?>
                <div class="callout callout-info">
                    <h4>Hi, <?php echo $_SESSION['fullname']?>!</h4>
                    <p>Selamat datang kembali di ACE - Bussiness Card Management.</p>
                </div>
            <?php
            }
            ?>

            <!-- Event Deskription -->
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-solid box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Event Detail</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-primary btn-sm" data-widget="collapse">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <table class="table table-condensed">
                                <tr>
                                    <td class="col-md-2">Senapan</td>
                                    <td> <?php echo $data->senapan ?> </td>
                                </tr>
                                <tr>
                                    <td>Kaliber</td>
                                    <td> <?php echo $data->ammo ?> </td>
                                </tr>
                                <tr>
                                    <td>Jarak</td>
                                    <td> <?php echo $data->jarak ?> </td>
                                </tr>
                                <tr>
                                    <td>Max. Shots</td>
                                    <td> <?php echo $data->max_shots ?> </td>
                                </tr>
                                <tr>
                                    <td>Deskripsi</td>
                                    <td> <?php echo $data->deskripsi ?> </td>
                                </tr>
                                <tr>
                                    <td>Tanggal</td>
                                    <td> <?php echo $data->create ?> </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div> <!-- event detail -->
                <!-- Summary / Result -->
                <?php
                if (count($getShooter) > 0){
                    foreach ($getShooter as $user){
                        ?>
                        <div class="col-md-6">
                            <div class="box box-solid box-info">
                                <div class="box-header">
                                    <h3 class="box-title">Shooter</h3>
                                    <div class="box-tools pull-right">
                                        <button class="btn btn-primary btn-sm" data-widget="collapse">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <table class="table table-condensed">
                                        <tr>
                                            <td class="col-md-2">Nama</td>
                                            <td> <?php echo $user->nama_lengkap ?> </td>
                                        </tr>
                                        <tr>
                                            <td>Kesatuan</td>
                                            <td> <?php echo $user->kesatuan ?> </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div><!-- /summary -->
                    <?php
                    }
                }
                ?>
            </div> <!-- data -->

            <div class="row">
                <div class="col-md-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li><a href="#latihan" role="tab" data-toggle="tab">Practice</a></li>
                        <li class="active"><a href="#match" role="tab" data-toggle="tab">Real Match</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane" id="latihan">
                            <div class="table-responsive">
                                <table style="margin-top:5px;" class="table table-striped">
                                    <tr>
                                        <th>#</th>
                                        <th>Waktu</th>
                                        <th>Skor</th>
                                    </tr>
                                    <?php
                                    if (count($getWarming) > 0){
                                        $skor2=0;
                                        foreach ($getWarming as $nilai1){
                                            ?>
                                            <tr>
                                                <td><?php echo $nilai1->no_urut ?></td>
                                                <td><?php echo $nilai1->created ?></td>
                                                <td><?php echo $nilai1->nilai ?></td>
                                            </tr>
                                            <?php
                                            $skor2=$skor2+$nilai1->nilai;
                                        }
                                        $akurasi2=($skor2/count($getWarming))*10;
                                        ?>
                                        <tr>
                                            <td colspan="2"><strong>Total Skor</strong></td>
                                            <td><?php echo $skor2 ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><strong>Akurasi Tembakan</strong></td>
                                            <td><?php echo $akurasi2?>%</td>
                                        </tr>
                                        <?php
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="3">Data tidak ada !</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </div>
                        </div><!-- /latihan -->
                        <div class="tab-pane active" id="match">
                            <div class="table-responsive">
                                <table style="margin-top:5px;" class="table table-striped">
                                    <tr>
                                        <th>#</th>
                                        <th>Waktu</th>
                                        <th>Skor</th>
                                    </tr>
                                    <?php
                                    if (count($getSReal) > 0){
                                        $skor=0;
                                        foreach ($getSReal as $nilai2){
                                            ?>
                                            <tr>
                                                <td><?php echo $nilai2->no_urut ?></td>
                                                <td><?php echo $nilai2->created ?></td>
                                                <td><?php echo $nilai2->nilai ?></td>
                                            </tr>
                                            <?php
                                            $skor=$skor+$nilai2->nilai;
                                        }
                                        $akurasi=($skor/count($getSReal))*10;
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="2"><strong>Total Skor</strong></td>
                                        <td><?php echo $skor ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><strong>Akurasi Tembakan</strong></td>
                                        <td><?php echo $akurasi?>%</td>
                                    </tr>
                                </table>
                            </div>
                        </div><!-- /real match -->
                    </div>
                    <a class="btn btn-primary" onclick="self.history.back();">Kembali</a>
                </div>
            </div>
        </section><!-- /.content -->
        <?php
    }
}
?>