	<!-- User Data -->
	<?php 
	if (count($getData) != 0){
	?>
	<div style="margin-top: 5px;" class="table-responsive">
		<table class="table table-hover" style="font-size: 13px; border-bottom: 1px solid #d9d9d9">
			<tr>
				<th>#</th>
				<th>Username</th>
				<th>Role</th>
				<th>Status</th>
				<th></th>
			</tr>
			<?php 
			$i = $num+1;
			foreach ($getData as $r){
			?>
			<tr>
				<td><?php echo $i?></td>
				<td><?php echo $r->username?></td>
				<td><?php echo $Role[$r->role_id]?></td>
				<td><div class="label <?php echo $Label[$r->is_active] ?>"><?php echo $Status[$r->is_active]?></div></td>
				<td>
					<div class="pull-right">
						<?php /**<a href="<?php echo site_url('user/detail/'.$r->id)?>"><i class="glyphicon glyphicon-list-alt"></i></a>&nbsp;**/?>
						<a href="<?php echo site_url('user/delete/'.$r->id.'/'.$r->sid)?>" title="delete" onclick="return confirm('Anda yakin?');"><i class="glyphicon glyphicon-remove"></i></a>&nbsp;
						<a href="<?php echo site_url('user/edit/'.$r->id.'/'.$r->sid)?>" title="edit"><i class="glyphicon glyphicon-check"></i></a>
					</div>
				</td>
			</tr>
			<?php
			$i++;
			}
			?>
		</table>
	</div>
	<?php 
	}
	?>