<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Tambah User</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Users</a></li>
	</ol>
</section> <!-- /content-header -->

<!-- Main content -->
<section class="content">
	<form role="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('user/simpan')?>">
        <div class="row form-group">
            <div class="col-md-4">
                <label>Username *</label>
                <input id="username" class="form-control" type="text" name="username" placeholder="max min 6 character *" required />
            </div>
            <div class="col-md-4">
                <label>Password</label>
                <input class="form-control" type="password" name="password" placeholder="*******" required>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4">
                <label>Role</label>
                <select name="role_id" class="form-control" data-placeholder="Pilih Role User...">
                    <?php
                    if (count($Role)){
                        foreach ($Role as $k => $v){
                        ?>
                            <option value="<?php echo $k ?>"><?php echo $v ?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-4">
                <label>Status</label>
                <select name="is_active" class="form-control">
                    <?php
                    if (count($Status)){
                        foreach ($Status as $k => $v){
                        ?>
                            <option value="<?php echo $k ?>"><?php echo $v ?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
		<div class="pull-left">
			<button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i>&nbsp;Simpan</button>
			<button class="btn btn-danger btn-flat" type="button" onclick="self.history.back();"><i class="fa fa-times"></i>&nbsp;Batal</button>
		</div>
	</form>
</section><!-- /.content -->