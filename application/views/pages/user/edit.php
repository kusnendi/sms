<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Edit User</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Users</a></li>
	</ol>
</section> <!-- /content-header -->

<!-- Main content -->
<section class="content">
    <?php
    if (count($getData) > 0){
        foreach ($getData as $data){
            ?>
            <form role="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('user/update')?>">
                <input type="hidden" name="id" value="<?php echo $data->id ?>">
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Username *</label>
                        <input id="username" class="form-control" type="text" name="username" value="<?php echo $data->username ?>" placeholder="max min 6 character *" required />
                    </div>
                    <div class="col-md-4">
                        <label>Password</label>
                        <input class="form-control" type="password" name="password" placeholder="*******">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Role</label>
                        <select name="role_id" class="form-control">
                            <?php
                            if (count($Role)){
                                foreach ($Role as $k => $v){
                                    if ($k == $data->role_id){
                                        ?>
                                        <option value="<?php echo $k ?>" selected><?php echo $v ?></option>
                                        <?php
                                    } else {
                                        ?>
                                        <option value="<?php echo $k ?>"><?php echo $v ?></option>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <label>Status</label>
                        <select name="is_active" class="form-control">
                            <?php
                            if (count($Status)){
                                foreach ($Status as $k => $v){
                                    if ($k == $data->is_active){
                                        ?>
                                        <option value="<?php echo $k ?>" selected><?php echo $v ?></option>
                                    <?php
                                    } else {
                                        ?>
                                        <option value="<?php echo $k ?>"><?php echo $v ?></option>
                                    <?php
                                    }
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        <h5>Catatan:</h5>
                        *) Jika password tidak di ganti, kosongkan saja.
                    </div>
                </div>
                <div class="pull-left">
                    <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i>&nbsp;Simpan</button>
                    <button class="btn btn-danger btn-flat" type="button" onclick="self.history.back();"><i class="fa fa-times"></i>&nbsp;Batal</button>
                </div>
            </form>
            <?php
        }
    }
    ?>
</section><!-- /.content -->