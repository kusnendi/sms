<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Edit Shooter</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Shooter</a></li>
	</ol>
</section> <!-- /content-header -->

<!-- Main content -->
<section class="content">
    <?php
    if (count($getData) > 0){
        foreach ($getData as $data){
            ?>
            <form role="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('shooter/simpan')?>">
                <h4>Biodata Penembak</h4>
                <div class="row form-group">
                    <div class="col-md-4">
                        <input id="nama_lengkap" class="form-control" type="text" name="namalengkap" value="<?php echo $data->nama_lengkap ?>" placeholder="Nama Lengkap *" required />
                    </div>
                    <div class="col-md-4">
                        <select class="form-control" name="kesatuan_id">
                            <option value='' selected>Kesatuan</option>
                            <?php
                            if (count($getKesatuan) > 0){
                                foreach ($getKesatuan as $k){
                                    if ($k->id == $data->kesatuan_id){
                                        ?>
                                        <option value="<?php echo $k->id?>" selected><?php echo $k->nama?></option>
                                        <?php
                                    } else {
                                        ?>                                    }
                                        <option value="<?php echo $k->id?>"><?php echo $k->nama?></option>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4">
                        <input class="form-control" type="text" name="handphone" value="<?php echo $data->handphone ?>" placeholder="Handphone *" required>
                    </div>
                    <div class="col-md-4">
                        <input class="form-control" type="text" name="email" value="<?php echo $data->email ?>" placeholder="Email">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-8">
                        <input id="alamat" class="form-control" type="text" name="alamat" value="<?php echo $data->alamat ?>" placeholder="Alamat">
                    </div>
                </div>
                <div class="pull-left">
                    <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i>&nbsp;Simpan</button>
                    <button class="btn btn-danger btn-flat" type="button" onclick="self.history.back();"><i class="fa fa-times"></i>&nbsp;Batal</button>
                </div>
            </form>
            <?php
        }
    }
    ?>
</section><!-- /.content -->