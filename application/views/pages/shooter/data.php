	<!-- User Data -->
	<?php 
	if (count($getData) != 0){
	?>
	<div style="margin-top: 5px;" class="table-responsive">
		<table class="table table-hover" style="font-size: 13px; border-bottom: 1px solid #d9d9d9">
			<tr>
				<th>#</th>
				<th>Nama</th>
				<th>Handphone</th>
				<th>Email</th>
				<th>Alamat</th>
                <th>Kesatuan</th>
				<th></th>
			</tr>
			<?php 
			$i = $num+1;
			foreach ($getData as $r){
			?>
			<tr>
				<td><?php echo $i?></td>
				<td><?php echo $r->nama_lengkap?></td>
				<td><?php echo $r->handphone?></td>
				<td><?php echo $r->email?></td>
				<td><?php echo $r->alamat?></td>
                <td><?php echo $r->kesatuan?></td>
				<td>
					<div class="pull-right">
						<?php /**<a href="<?php echo site_url('shooter/detail/'.$r->id)?>"><i class="glyphicon glyphicon-list-alt"></i></a>&nbsp;**/?>
						<a href="<?php echo site_url('shooter/delete/'.$r->id)?>" title="delete" onclick="return confirm('Anda yakin?');"><i class="glyphicon glyphicon-remove"></i></a>&nbsp;
						<a href="<?php echo site_url('shooter/edit/'.$r->id)?>" title="edit"><i class="glyphicon glyphicon-check"></i></a>
					</div>
				</td>
			</tr>
			<?php
			$i++;
			}
			?>
		</table>
	</div>
	<?php 
	}
	?>