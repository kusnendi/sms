<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>ACE | Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url('assets/css/AdminLTE.css')?>" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
            <div class="header">MS-126</div>
            <form action="<?php echo site_url('login/user_auth')?>" method="post">
                <div class="body bg-gray">
                    <?php 
                    if (isset($_GET['msg']) == 'failed'){
					?>
						<div class="callout callout-danger">
							<h4>Login Gagal !</h4>
							<p>Username atau password yang anda masukan salah, silahkan login ulang!</p>
						</div>
					<?php
					} unset($_GET['msg']);
                    ?>
                    <div class="form-group">
                        <input type="text" name="username" class="form-control" placeholder="User ID" autofocus required/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password" required/>
                    </div>
                    <?php 
                    /****          
                    <div class="form-group">
                        <input type="checkbox" name="remember_me"/> Remember me
                    </div>
                    ****/
                    ?>
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-olive btn-block btn-flat"><i class="fa fa-sign-in"></i>&nbsp;Masuk</button>
                    <p><a href="#">Lupa kata sandi?</a></p>
                </div>
            </form>
        </div>


        <!-- jQuery 2.0.2 -->
        <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>" type="text/javascript"></script>        

    </body>
</html>