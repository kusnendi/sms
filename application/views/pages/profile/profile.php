<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>User Profile</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Profile</a></li>
    </ol>
</section>

<!-- Main Content -->
<section class="content">
    <?php
    if (count($getData)){
        foreach ($getData as $user){
            ?>
            <div class="row"> <!-- Row Profile -->
                <!-- Profile Picture -->
                <div class="col-md-3">
                    <div class="box">
                        <div class="box-body">
                            <div class="text-center">
                                <img src="<?php echo base_url('assets/img/users/'.$_SESSION['display']) ?>" class="img-responsive center-block">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Profile Information -->
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-body">
                            <form method="post" action="<?php echo site_url('profile/update/'.$user->id) ?>">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input class="form-control" type="text" name="nama" placeholder="Nama Lengkap" value="<?php echo $user->nama_lengkap ?>" required>
                                </div>
                                <div class="form-group">
                                    <label>Handphone</label>
                                    <input class="form-control" type="text" name="handphone" value="<?php echo $user->handphone ?>" required>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input class="form-control" type="email" name="email" value="<?php echo $user->email ?>">
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea name="alamat" class="form-control"><?php echo $user->alamat ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Kesatuan</label>
                                    <select name="kesatuan_id" class="form-control">
                                        <?php
                                        if (count($Kesatuan)){
                                            foreach ($Kesatuan as $k){
                                                if ($user->kesatuan_id == $k->id){
                                                    ?>
                                                    <option value="<?php echo $k->id?>" selected><?php echo $k->nama?></option>
                                                <?php
                                                } else {
                                                    ?>
                                                    <option value="<?php echo $k->id?>"><?php echo $k->nama?></option>
                                                <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    } else {
        ?>
        <div class="row"> <!-- Row Profile -->
            <!-- Profile Picture -->
            <div class="col-md-3">
                <div class="box">
                    <div class="box-body">
                        <div class="text-center">
                            <img src="<?php echo base_url('assets/img/users/default.gif') ?>" class="img-responsive center-block">
                        </div>
                    </div>
                </div>
            </div>
            <!-- Profile Information -->
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-body">
                        <form method="post" action="<?php echo site_url('profile/simpan') ?>">
                            <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id'] ?>">
                            <div class="form-group">
                                <label>Nama</label>
                                <input class="form-control" type="text" name="nama" placeholder="Nama Lengkap" value="<?php echo $_SESSION['fullname'] ?>" required>
                            </div>
                            <div class="form-group">
                                <label>Handphone</label>
                                <input class="form-control" type="text" name="handphone" required>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control" type="email" name="email" >
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea name="alamat" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Kesatuan</label>
                                <select name="kesatuan_id" class="form-control">
                                    <?php
                                    if (count($Kesatuan)){
                                        foreach ($Kesatuan as $k){
                                            ?>
                                            <option value="<?php echo $k->id?>" selected><?php echo $k->nama?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</section>