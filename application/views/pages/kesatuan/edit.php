<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Tambah Kesatuan</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Kesatuan</a></li>
	</ol>
</section> <!-- /content-header -->

<!-- Main content -->
<section class="content">
    <?php
    if (count($getData) > 0){
        foreach ($getData as $k){
            ?>
            <form role="form" method="post" action="<?php echo site_url('kesatuan/update')?>">
                <input type="hidden" name="id" value="<?php echo $k->id ?>">
                <h4>Kesatuan</h4>
                <div class="row form-group">
                    <div class="col-md-8">
                        <input id="judul" class="form-control" type="text" name="nama" value="<?php echo $k->nama; ?>" placeholder="Unit kesatuan *" required />
                    </div>
                </div>
                <h4>Alamat</h4>
                <div class="row form-group">
                    <div class="col-md-8">
                        <textarea class="form-control" name="alamat"><?php echo $k->alamat; ?></textarea>
                    </div>
                </div>
                <div class="pull-left">
                    <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i>&nbsp;Simpan</button>
                    <button class="btn btn-danger btn-flat" onclick="self.history.back(-1);" type="button"><i class="fa fa-times"></i>&nbsp;Batal</button>
                </div>
            </form>
            <?php
        }
    }
    ?>
</section><!-- /.content -->