	<!-- User Data -->
	<?php 
	if (count($getData) != 0){
	?>
	<div style="margin-top: 5px;" class="table-responsive">
		<table class="table table-hover" style="font-size: 13px; border-bottom: 1px solid #d9d9d9">
			<tr>
				<th>#</th>
				<th>Unit</th>
				<th>Alamat</th>
				<th></th>
			</tr>
			<?php 
			$i = $num+1;
			foreach ($getData as $r){
			?>
			<tr>
				<td><?php echo $i?></td>
				<td><?php echo $r->nama?></td>
				<td><?php echo $r->alamat?></td>
				<td>
					<div class="pull-right">
						<a href="<?php echo site_url('kesatuan/delete/'.$r->id)?>" title="delete" onclick="return confirm('Anda yakin?');"><i class="glyphicon glyphicon-remove"></i></a>&nbsp;
						<a href="<?php echo site_url('kesatuan/edit/'.$r->id)?>" title="edit"><i class="glyphicon glyphicon-check"></i></a>
					</div>
				</td>
			</tr>
			<?php
			$i++;
			}
			?>
		</table>
        <?php echo $hal; ?>
	</div>
	<?php 
	}
	?>