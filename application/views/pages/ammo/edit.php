<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Tambah Ammo</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Ammo</a></li>
	</ol>
</section> <!-- /content-header -->

<!-- Main content -->
<section class="content">
    <?php
    if (count($getData) > 0){
        foreach ($getData as $a){
            ?>
            <form role="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('ammo/update')?>">
                <input type="hidden" name="id" value="<?php $a->id; ?>">
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Nama</label>
                        <input id="name" class="form-control" type="text" name="name" value="<?php echo $a->name; ?>" placeholder="Nama Ammo *" required />
                    </div>
                    <div class="col-md-8">
                        <label>Tipe</label>
                        <input id="type" class="form-control" type="text" name="type" value="<?php echo $a->type; ?>" placeholder="Jenis Ammo *"/>
                    </div>
                </div>
                <div class="pull-left">
                    <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i>&nbsp;Simpan</button>
                    <button class="btn btn-danger btn-flat" type="button" onclick="self.history.back();"><i class="fa fa-times"></i>&nbsp;Batal</button>
                </div>
            </form>
            <?php
        }
    }
    ?>
</section><!-- /.content -->