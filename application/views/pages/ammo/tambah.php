<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Tambah Ammo</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Ammo</a></li>
	</ol>
</section> <!-- /content-header -->

<!-- Main content -->
<section class="content">
	<form role="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('ammo/simpan')?>">
		<div class="row form-group">
			<div class="col-md-4">
                <label>Nama</label>
	          	<input id="name" class="form-control" type="text" name="name" placeholder="Nama Ammo *" required />
			</div>

            <div class="col-md-4">
                <label>Tipe</label>
                <input id="type" class="form-control" type="text" name="type" placeholder="Jenis Ammo *" />
            </div>
		</div>
		<div class="pull-left">
			<button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i>&nbsp;Simpan</button>
			<button class="btn btn-danger btn-flat" type="button" onclick="self.history.back();"><i class="fa fa-times"></i>&nbsp;Batal</button>
		</div>
	</form>
</section><!-- /.content -->