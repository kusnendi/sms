<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Tambah Rifle</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Rifle</a></li>
	</ol>
</section> <!-- /content-header -->

<!-- Main content -->
<section class="content">
	<form role="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('rifle/simpan')?>">
		<div class="row form-group">
			<div class="col-md-4">
                <label>Name *</label>
	          	<input id="judul" class="form-control" type="text" name="senapan" placeholder="Nama Senapan *" required />
			</div>
            <div class="col-md-4">
                <label>Caliber *</label>
               <select name="caliber" class="form-control">
                    <option name="caliber" value="n/a" selected>Any</option>
                   <?php
                   if (count($getAmmo) > 0){
                       foreach ($getAmmo as $a){
                           ?>
                           <option value="<?php echo $a->name ?>"><?php echo $a->name ?></option>
                           <?php
                       }
                   }
                   ?>
                   ?>
               </select>
            </div>
		</div>

        <div class="row form-group">
            <div class="col-md-4">
                <label>Weight</label>
                <input class="form-control" type="text" name="weight" placeholder="dalam satuan gr"/>
            </div>
            <div class="col-md-4">
                <label>Length</label>
                <input class="form-control" type="text" name="length" placeholder="dalam satuan mm"/>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-md-4">
                <label>Barrel Length</label>
                <input class="form-control" type="text" name="barrel" placeholder="dalam satuan mm"/>
            </div>
            <div class="col-md-4">
                <label>Magazine Capacity</label>
                <input class="form-control" type="text" name="capacity" placeholder="dalam satuan rounds"/>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-md-8">
                <label>Gambar</label>
                <input class="form-control" type="file" name="picture" />
            </div>
        </div>

		<div class="row form-group">
			<div class="col-md-8">
                <label>Catatan</label>
				<textarea class="form-control" name="catatan" placeholder="Ketik catatan disini..."></textarea>
			</div>
		</div>
		<div class="pull-left">
			<button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i>&nbsp;Simpan</button>
			<button class="btn btn-danger btn-flat" onclick="self.history.back();" type="button"><i class="fa fa-times"></i>&nbsp;Batal</button>
		</div>
	</form>
</section><!-- /.content -->