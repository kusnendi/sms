	<!-- User Data -->
	<?php 
	if (count($getData) != 0){
	?>
	<div style="margin-top: 5px;" class="table-responsive">
		<table class="table table-hover" style="font-size: 13px; border-bottom: 1px solid #d9d9d9">
			<tr>
				<th class="col-md-2"></th>
				<th>Description</th>
				<th>Note</th>
				<th></th>
			</tr>
			<?php 
			$i = $num+1;
			foreach ($getData as $r){
			?>
			<tr>
				<td><img src="<?php echo base_url('assets/img/rifle/'.$r->picture)?>" width="150" height="100"></td>
				<td>
                    Name : <?php echo $r->name?><br>Caliber : <?php echo $r->caliber?><br>Weight : <?php echo $r->weight?><br>
                    Length : <?php echo $r->length?><br>Barrel Length : <?php echo $r->barrel_length?><br>
                    Magazine Capacity : <?php echo $r->capacity?><br>
                </td>
				<td><?php echo $r->note?></td>
				<td>
					<div class="pull-right">
						<?php /**<a href="<?php echo site_url('rifle/detail/'.$r->id)?>"><i class="glyphicon glyphicon-list-alt"></i></a>&nbsp;**/?>
						<a href="<?php echo site_url('rifle/delete/'.$r->id)?>" title="delete" onclick="return confirm('Anda yakin?');"><i class="glyphicon glyphicon-remove"></i></a>&nbsp;
						<a href="<?php echo site_url('rifle/edit/'.$r->id)?>" title="edit"><i class="glyphicon glyphicon-check"></i></a>
					</div>
				</td>
			</tr>
			<?php
			$i++;
			}
			?>
		</table>
        <?php echo $hal; ?>
	</div>
	<?php 
	}
	?>