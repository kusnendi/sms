<?php
if (count($getData)){
    $i = 1;
    foreach ($getData as $data){
        ?>
        <div class="col-md-4">
            <div class="box box-solid box-primary">
                <div class="box-header">
                    <h3 class="box-title"><?php echo $data->name ?></h3>
                    <div class="box-tools pull-right">
                        #<?php echo $i ?>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <?php //<img class="img-thumbnail img-responsive" src="<?php echo base_url('assets/img/users/'.$data->photo)"> ?>
                            <div class="icon" style="font-size:96px">
                                <i class="ion ion-person"></i>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="inner text-center">
                                <p>Score</p>
                                <h3><?php echo $data->score ?></h3>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="inner text-center">
                                <p>Accuracy</p>
                                <h3><?php echo $data->accuracy ?>%</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
        $i++;
    }
}
?>