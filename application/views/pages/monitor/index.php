<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>SMS | ACE</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- bootstrap 3.0.2 -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
    <!-- font Awesome -->
    <link href="<?php echo base_url('assets/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="<?php echo base_url('assets/css/ionicons.min.css')?>" rel="stylesheet" type="text/css" />
    <!-- CHosen -->
    <link href="<?php echo base_url('assets/js/plugins/chosen/chosen.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/js/plugins/chosen/chosen-bootstrap.css')?>" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url('assets/css/AdminLTE.css')?>" rel="stylesheet" type="text/css" />

    <!-- jQuery 2.0.2 -->
    <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>

    <!-- Select2 -->
    <?php //<script src="http://harvesthq.github.io/chosen/chosen.jquery.js"></script> ?>
    <?php /***
    <script>
    $(function() {
    $('.select2').chosen();
    $('.select2-deselect').chosen({ allow_single_deselect: true });
    });
    </script> ***/
    ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<header class="header">
    <a href="#" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        Shots Monitoring
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
    </nav>
</header>

<section class="content">
    <div class="row">
        <div class="result"></div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function(){
        done();
    });

    function done() {
        setTimeout( function() {
            loadData();
            done();
        }, 500);
    }

    function loadData(){
        $.get( "<?php echo site_url('monitor/ajaxrequest') ?>", function( data ) {
            $(".result").html( data );
        });
    }
</script>