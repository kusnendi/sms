<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Range</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Range</a></li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Pesan Notifikasi -->
	<?php 
	if (@$_GET['msg_id'] == 401){
	?>
		<div class="callout callout-danger">
			<h4>Hi, <?php echo $_SESSION['fullname']?>!</h4>
			<p>Data berhasil di hapus, data yang telah di hapus tidak bisa di restore.</p>
		</div>
	<?php
	} elseif (@$_GET['msg_id'] == 402) {
	?>
		<div class="callout callout-info">
			<h4>Hi, <?php echo $_SESSION['fullname']?>!</h4>
			<p>Data berhasil di simpan, lihat pada daftar data.</p>
		</div>
	<?php
	} elseif (@$_GET['msg_id'] == 405){
	?>
		<div class="callout callout-info">
			<h4>Hi, <?php echo $_SESSION['fullname']?>!</h4>
			<p>Selamat datang kembali di ACE - Bussiness Card Management.</p>
		</div>
	<?php
	}
	?>
	
	<!-- Kotak Pencarian -->
	<div class="callout callout-info">
		<form class="form" action="">
			<h4>Pencarian Cepat</h4>
			<div class="row form-group">
				<div class="col-md-3">
					<select class="form-control" name="q">
						<option value="any">Any</option>
					</select>
				</div>
				<div class="col-md-9">
					<input class="form-control" type="text" name="keyword" placeholder="Ketik kata kunci..">
				</div>
			</div>
			<button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-search"></i>&nbsp;&nbsp;Cari</button>
		</form>
	</div>
	
	<!-- Add Button -->
	<a href="<?php echo site_url('range/add')?>" class="btn btn-primary btn-flat pull-right" style="margin-bottom:5px;"><i class="fa fa-plus-square"></i>&nbsp;Tambah Data</a>
	<?php $this->load->view('pages/range/data')?>
	
</section><!-- /.content -->