<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Tambah Range</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Range</a></li>
	</ol>
</section> <!-- /content-header -->

<!-- Main content -->
<section class="content">
	<form role="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('range/simpan')?>">
		<h4>Jarak</h4>
		<div class="row form-group">
			<div class="col-md-8">
	          	<input id="jarak" class="form-control" type="text" name="jarak" placeholder="dalam satuan meter *" required />
			</div>
		</div>
		<div class="pull-left">
			<button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i>&nbsp;Simpan</button>
			<button class="btn btn-danger btn-flat" type="button"><i class="fa fa-times"></i>&nbsp;Batal</button>
		</div>
	</form>
</section><!-- /.content -->