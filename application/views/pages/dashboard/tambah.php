<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Tambah Data eCard</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	</ol>
</section> <!-- /content-header -->

<!-- Main content -->
<section class="content">
	<form role="form" enctype="multipart/form-data" method="post" action="<?php echo site_url('dashboard/simpan')?>">
		<h4>Biodata</h4>
		<div class="row form-group">
			<div class="col-md-8">
				<div class="input-group">
	          		<input id="nama_lengkap" class="form-control" type="text" name="namalengkap" placeholder="Nama Lengkap *" required />
	          		<span class="input-group-btn">
	          			<a class="btn" onclick="check_data();" data-toggle="modal" href="#existeduser"><i class="fa fa-check"></i></a>
	          		</span>
          		</div>
			</div>
			<div class="col-md-4">
				<input class="form-control" type="text" name="namapanggilan" placeholder="Nama Panggilan">
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-4">
				<input class="form-control" type="text" name="mobile" placeholder="Handphone 1 *" required>	
			</div>
			<div class="col-md-4">
				<input class="form-control" type="text" name="mobile2" placeholder="Handphone 2">
			</div>
			<div class="col-md-4">
				<input class="form-control" type="email" name="email" placeholder="Email">
			</div>
		</div>
		<h4>Company</h4>
		<div class="row form-group">
			<div class="col-md-4">
				<input id="company" class="form-control" type="text" name="company" placeholder="Nama Perusahaan *" required>
			</div>
			<div class="col-md-4">
				<select id="industri" name="industri_id" class="form-control">
					<option value=''> Pilih Industri </option>
					<?php 
					if (!empty($Industri)){
						foreach ($Industri as $I){
						?>
							<option value="<?php echo $I->ID_INDUSTRY?>"><?php echo $I->TYPE_INDUSTRY?></option>
						<?php 
						}
					}
					?>
				</select>
			</div>
			<div class="col-md-4">
				<input class="form-control" type="text" name="jabatan" placeholder="Jabatan">
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-8">
				<input id="alamat" class="form-control" type="text" name="alamat" placeholder="Alamat">
			</div>
			<div class="col-md-4">
				<select name="kota_id" class="form-control">
					<option value=''> Pilih Kota </option>
					<?php 
					if (!empty($Kota)){
						foreach ($Kota as $K){
							if($K->ID_KOTA == 38){
							?>
								<option value="<?php echo $K->ID_KOTA?>" selected><?php echo $K->NM_KOTA?></option>
							<?php
							} else {
							?>
								<option value="<?php echo $K->ID_KOTA?>"><?php echo $K->NM_KOTA?></option>
							<?php	
							} 
						}
					}
					?>
				</select>
				</select>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-3">
				<input class="form-control" type="text" name="telp" placeholder="Telp">
			</div>
			<div class="col-md-1">
				<input class="form-control" type="text" name="ext" placeholder="Ext.">
			</div>
			<div class="col-md-4">
				<input class="form-control" type="text" name="fax" placeholder="Fax">
			</div>
			<div class="col-md-4">
				<input class="form-control" type="text" name="website" placeholder="Website">
			</div>
		</div>
		<h4>Catatan</h4>
		<div class="form-group">
			<textarea class="form-control" name="catatan" placeholder="note's here..."></textarea>
		</div>
		<h4>Attachment</h4>
		<div class="form-group">
			<input class="form-control" type="file" name="fupload">
		</div>
		<div class="pull-right">
			<button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i>&nbsp;Simpan</button>
			<button class="btn btn-danger btn-flat" type="button"><i class="fa fa-times"></i>&nbsp;Batal</button>
		</div>
	</form>
</section><!-- /.content -->
<!-- load modal existed user -->
<?php //$this->load->view('pages/dashboard/modal_existed_user');?>

		<div id="existeduser" class="modal fade">
          	<div class="modal-dialog">
          		<div class="modal-content">
          			<div class="modal-header">
          				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          				<h4 class="modal-title"><i class="fa fa-check-square"></i>&nbsp;Check Data</h4>
          			</div>
          			
          			<div class="modal-body">
          				<div id="listuser" class="existeduser"></div>
          			</div>
          		</div>
          	</div>
        </div>

<script type="text/javascript">
function check_data(){
	var nmUser = $('#nama_lengkap').val();
	
	$.ajax({
		type: "POST",
		url : "<?php echo site_url('dashboard/is_existed');?>",
		data: "nmlengkap="+nmUser,
		cache:false,
		success: function(data){
			//alert(data);return;
			$('#listuser').html(data);
		}
	})
	
	//alert(nmUser);
}
</script>