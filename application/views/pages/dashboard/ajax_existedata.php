<?php 
if (count($getData) != 0){
	?>
	<p>Berikut data yang sudah terdaftar di database.</p>
	<table class="table" style="font-size: 11px">
	  <tr>
	    <th>Nama Lengkap</th>
	    <th>Perusahaan</th>
	    <th>&nbsp;</th>
	  </tr>
	  <?php 
	  foreach ($getData as $row){
		?>
		<tr>
			<td><?php echo $row->nama_lengkap?></td>
			<td><?php //echo $row->company?></td>
			<td>
				<div class="pull-right">
					<!-- <a href="<?php //echo site_url('dashboard/detail/'.$r->id)?>"><i class="glyphicon glyphicon-list-alt"></i></a>&nbsp;  -->
					<a href="<?php //echo site_url('dashboard/edit/'.$row->id)?>" title="edit"><i class="glyphicon glyphicon-check"></i></a>
				</div>
			</td>
		</tr>
		<?php
	  }
	  ?>
	</table>
	<?php
} else {
?>
	<div class="callout callout-danger">
		<h4>Hi, <?php echo $_SESSION['fullname']?>!</h4>
		<p>Data tidak ditemukan! Data belum terdaftar di database, terimkasih.</p>
	</div>
<?php
}
?>