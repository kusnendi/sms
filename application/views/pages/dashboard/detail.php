<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Informasi IDCard</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	</ol>
</section>

<?php 
if (count($getData) != 0){
?>
<!-- Main content -->
<section class="content">
	<div class="row">
		<?php 
		foreach ($getData as $r){
		?>
		<!-- load image card -->		
		<div class="col-md-6">
			<div class="box box-danger">
				<div class="box-body">
					<img class="img-responsive" alt="Detail Card" src="<?php echo base_url('files/card/'.$r->file)?>">
				</div>
			</div>
		</div>
		<!-- view detail profil -->
		<div class="col-md-6">
			<div class="box box-info">
				<div class="box-header">
					<i class="fa fa-th"></i>
					<h3 class="box-title">Detail Informasi</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-4">
							<span style="font-size: 14px; font-weight: bold; color: #333">Nama Lengkap</span>
						</div>
						<div class="col-md-8">
						<?php echo $r->nama_lengkap?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<span style="font-size: 14px; font-weight: bold; color: #333">Nama Panggilan</span>
						</div>
						<div class="col-md-8">
						<?php echo $r->nama_panggilan?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<span style="font-size: 14px; font-weight: bold; color: #333">Handphone 1</span>
						</div>
						<div class="col-md-8">
						<?php echo $r->mobile?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<span style="font-size: 14px; font-weight: bold; color: #333">Handphone 2</span>
						</div>
						<div class="col-md-8">
						<?php echo $r->mobile2?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<span style="font-size: 14px; font-weight: bold; color: #333">Email</span>
						</div>
						<div class="col-md-8">
						<?php echo $r->email?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<span style="font-size: 14px; font-weight: bold; color: #333">Nama Perusahaan</span>
						</div>
						<div class="col-md-8">
						<?php echo $r->company?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<span style="font-size: 14px; font-weight: bold; color: #333">Jabatan</span>
						</div>
						<div class="col-md-8">
						<?php echo $r->judul_posisi?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<span style="font-size: 14px; font-weight: bold; color: #333">Alamat</span>
						</div>
						<div class="col-md-8">
						<?php echo $r->alamat?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<span style="font-size: 14px; font-weight: bold; color: #333">Kota</span>
						</div>
						<div class="col-md-8">
						Jakarta
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<span style="font-size: 14px; font-weight: bold; color: #333">Telp</span>
						</div>
						<div class="col-md-3">
						<?php echo $r->telp?>
						</div>
						<div class="col-md-5">
						ext.&nbsp;&nbsp;&nbsp;<?php echo $r->ext?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<span style="font-size: 14px; font-weight: bold; color: #333">Fax</span>
						</div>
						<div class="col-md-4">
						<?php echo $r->fax?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<span style="font-size: 14px; font-weight: bold; color: #333">Website</span>
						</div>
						<div class="col-md-4">
						<?php echo $r->website?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<span style="font-size: 14px; font-weight: bold; color: #333">Catatan</span>
						</div>
						<div class="col-md-4">
						<?php echo $r->catatan?>
						</div>
					</div>
				</div>
			</div>
		</div><!-- /.detail-profil -->
		<?php 
		}
		?>
	</div>
</section><!-- /.content -->
<?php 
}
?>