<?php 
if (count($getData) != 0){
	foreach ($getData as $p){
		?>
		<div class="row form-group">
			<div class="col-md-6">
				<label>Nama</label>
				<input class="form-control" type="text" value="<?php echo $p->nama_lengkap?>" readonly>
			</div>
			<div class="col-md-6">
				<label>Jabatan</label>
				<input class="form-control" type="text" value="<?php echo $p->judul_posisi?>" readonly>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-6">
				<label>Company</label>
				<input class="form-control" type="text" value="<?php echo $p->company?>" readonly>
			</div>
			<div class="col-md-6">
				<label>Website</label>
				<input class="form-control" type="text" value="<?php echo $p->website?>" readonly>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-6">
				<label>Email</label>
				<input class="form-control" type="text" value="<?php echo $p->email?>" readonly>
			</div>
			<div class="col-md-6">
				<label>Mobile</label>
				<input class="form-control" type="text" value="<?php echo $p->mobile?>" readonly>
			</div>
		</div>
		<div class="row form-group">
			<div class="col-md-6">
				<label>Telp</label>
				<input class="form-control" type="text" value="<?php echo $p->telp?>" readonly>
			</div>
			<div class="col-md-6">
				<label>Fax</label>
				<input class="form-control" type="text" value="<?php echo $p->fax?>" readonly>
			</div>
		</div>
		<div class="form-group">
			<label>Catatan</label>
			<textarea class="form-control" readonly><?php echo $p->catatan?></textarea>
		</div>
		<?php
	}
}
?>