<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Quick Search</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<?php $this->load->view('pages/dashboard/data')?>
</section><!-- /.content -->

<div id="viewcard" class="modal fade">
	<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title"><i class="fa fa-user"></i>&nbsp;Kusnendi</h4>
		</div>
		<div class="modal-body">
			<div class="text-center">
				<img class="img-responsive" src="<?php echo base_url('files/card/img050.jpg')?>" alt="Card Detail">
			</div>
		</div>
	</div>
	</div>
</div>

<div id="profil" class="modal fade">
	<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title"><i class="fa fa-desktop"></i>&nbsp;Profil</h4>
		</div>
		<div class="modal-body">
			<div class="row form-group">
				<div class="col-md-6">
					<label>Nama</label>
					<input class="form-control" type="text" value="Kusnendi" readonly>
				</div>
				<div class="col-md-6">
					<label>Jabatan</label>
					<input class="form-control" type="text" value="Programmer" readonly>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-md-6">
					<label>Company</label>
					<input class="form-control" type="text" value="PT. Adicipta Carsani Ekakarya" readonly>
				</div>
				<div class="col-md-6">
					<label>Website</label>
					<input class="form-control" type="text" value="http://ace.co.id" readonly>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-md-6">
					<label>Email</label>
					<input class="form-control" type="text" value="nendi@ace.co.id" readonly>
				</div>
				<div class="col-md-6">
					<label>Mobile</label>
					<input class="form-control" type="text" value="08992332262" readonly>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-md-6">
					<label>Telp</label>
					<input class="form-control" type="text" value="0266731350" readonly>
				</div>
				<div class="col-md-6">
					<label>Fax</label>
					<input class="form-control" type="text" value="0266731350" readonly>
				</div>
			</div>
			<div class="form-group">
				<label>Catatan</label>
				<textarea class="form-control" readonly>It's Fine...</textarea>
			</div>
		</div>
	</div>
	</div>
</div>