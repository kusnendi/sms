	<!-- User Data -->
	<?php 
	if (count($getData) != 0){
	?>
	<div style="margin-top: 5px;" class="table-responsive">
		<table class="table table-hover" style="font-size: 13px; border-bottom: 1px solid #d9d9d9">
			<tr>
				<th>#</th>
				<th>NAMA</th>
				<th>JABATAN</th>
				<th>PERUSAHAAN</th>
				<th>EMAIL</th>
				<th>MOBILE</th>
				<th>OFFICE</th>
				<th></th>
			</tr>
			<?php 
			$i = $num+1;
			foreach ($getData as $r){
			?>
			<tr>
				<td><?php echo $i?></td>
				<td><a href="#viewcard" data-toggle="modal" onclick="viewcard(<?php echo $r->id?>);"><?php echo $r->nama_lengkap?></a></td>
				<td><?php echo $r->judul_posisi?></td>
				<td><?php echo $r->company?></td>
				<td><?php echo $r->email?></td>
				<td><?php echo $r->mobile?></td>
				<td><?php echo $r->telp?></td>
				<td>
					<div class="pull-right">
						<a href="<?php echo site_url('dashboard/detail/'.$r->id)?>"><i class="glyphicon glyphicon-list-alt"></i></a>&nbsp;
						<a href="<?php echo site_url('dashboard/delete/'.$r->id)?>" title="delete" onclick="return confirm('Anda yakin?');"><i class="glyphicon glyphicon-remove"></i></a>&nbsp;
						<a href="<?php echo site_url('dashboard/edit/'.$r->id)?>" title="edit"><i class="glyphicon glyphicon-check"></i></a>
					</div>
				</td>
			</tr>
			<?php
			$i++;
			}
			?>
		</table>
	</div>
	<?php 
	}
	?>