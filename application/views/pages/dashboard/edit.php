<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Edit IDCard</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	</ol>
</section>

<?php 
if (count($getData) != 0){
?>
<!-- Main content -->
<section class="content">
	<div class="row">
		<?php 
		foreach ($getData as $r){
		?>
		<!-- load image card -->		
		<div class="col-md-6">
			<div class="box box-danger">
				<div class="box-body">
					<img class="img-responsive" alt="Detail Card" src="<?php echo base_url('files/card/'.$r->file)?>">
				</div>
			</div>
		</div>
		<!-- view detail profil -->
		<div class="col-md-6">
			<div class="box box-info">
				<div class="box-header">
					<i class="fa fa-th"></i>
					<h3 class="box-title">Detail Informasi</h3>
				</div>
				<form enctype="multipart/form-data" method="post" action="<?php echo site_url('dashboard/update/'.$r->id) ?>">
				<input type="hidden" name="old_filename" value="<?php echo $r->file?>">
				<div class="box-body">
					<div class="row form-group">
						<div class="col-md-8">
							<input class="form-control" name="namalengkap" value="<?php echo $r->nama_lengkap?>" placeholder="Nama Lengkap *" required>
						</div>
						<div class="col-md-4">
							<input class="form-control" name="namapanggilan" value="<?php echo $r->nama_panggilan?>" placeholder="Nama Panggilan">
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-4">
							<input class="form-control" name="mobile" value="<?php echo $r->mobile?>" placeholder="Handphone 1" required>
						</div>
						<div class="col-md-4">
							<input class="form-control" name="mobile2" value="<?php echo $r->mobile2?>" placeholder="Handphone 2">
						</div>
						<div class="col-md-4">
							<input class="form-control" name="email" value="<?php echo $r->email?>" placeholder="Email">
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-4">
							<input class="form-control" name="company" value="<?php echo $r->company?>" placeholder="Nama Perusahaan" placeholder="Nama Perusahaan" required>
						</div>
						<div class="col-md-4">
							<input class="form-control" name="industri_id" value="<?php echo $r->industry_id?>">
						</div>
						<div class="col-md-4">
							<input class="form-control" name="jabatan" value="<?php echo $r->judul_posisi?>" placeholder="Jabatan">
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-8">
							<input class="form-control" name="alamat" value="<?php echo $r->alamat?>" placeholder="Alamat *">
						</div>
						<div class="col-md-4">
							<input class="form-control" name="kota_id" value="<?php echo $r->kota_id?>">
						</div>
					</div>
					<div class="row form-group">
						<div class="col-md-4">
							<input class="form-control" name="telp" value="<?php echo $r->telp?>" placeholder="Telp">
						</div>
						<div class="col-md-4">
							<input class="form-control" name="fax" value="<?php echo $r->fax?>" placeholder="Fax">
						</div>
						<div class="col-md-4">
							<input class="form-control" name="website" value="<?php echo $r->website?>" placeholder="Website">
						</div>
					</div>
					<div class="form-group">
						<textarea class="form-control" name="catatan" placeholder="Catatan"><?php echo $r->catatan?></textarea>
					</div>
					<div class="form-group">
						<input class="form-control" name="fupload" type="file">
					</div>
				</div>
				<div class="box-footer">
					<button class="btn btn-danger btn-flat" type="button"><i class="fa fa-reply"></i>&nbsp;Batal</button>
					<button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i>&nbsp;Update</button>
				</div>
				</form>
			</div>
		</div><!-- /.detail-profil -->
		<?php 
		}
		?>
	</div>
</section><!-- /.content -->
<?php 
}
?>