<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Dashboard</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<!-- Pesan Notifikasi -->
	<?php 
	if (@$_GET['msg_id'] == 401){
	?>
		<div class="callout callout-danger">
			<h4>Hi, <?php echo $_SESSION['fullname']?>!</h4>
			<p>Data berhasil di hapus, data yang telah di hapus tidak bisa di restore.</p>
		</div>
	<?php
	} elseif (@$_GET['msg_id'] == 402) {
	?>
		<div class="callout callout-info">
			<h4>Hi, <?php echo $_SESSION['fullname']?>!</h4>
			<p>Data berhasil di simpan, lihat pada daftar data.</p>
		</div>
	<?php
	} elseif (@$_GET['msg_id'] == 405){
	?>
		<div class="callout callout-info">
			<h4>Hi, <?php echo $_SESSION['fullname']?>!</h4>
			<p>Selamat datang kembali di ACE - Bussiness Card Management.</p>
		</div>
	<?php
	}
	?>

    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>
                        <?php echo $jml_shooter; ?>
                    </h3>
                    <p>
                        Shooter
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo site_url('shooter') ?>" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>
                        <?php echo $jml_event; ?><!--<sup style="font-size: 20px">%</sup>-->
                    </h3>
                    <p>
                        Events
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-calendar"></i>
                </div>
                <a href="<?php echo site_url('event') ?>" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        <?php echo $jml_kesatuan; ?>
                    </h3>
                    <p>
                        Kesatuan
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-friends"></i>
                </div>
                <a href="<?php echo site_url('kesatuan') ?>" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>
                        <?php echo $jml_rifle; ?>
                    </h3>
                    <p>
                        Rifle
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-locate"></i>
                </div>
                <a href="<?php echo site_url('rifle') ?>" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->


</section><!-- /.content -->